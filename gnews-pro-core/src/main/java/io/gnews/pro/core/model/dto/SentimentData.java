package io.gnews.pro.core.model.dto;

/**
 * @author masasdani
 * Created Date Nov 3, 2015
 */
public class SentimentData {

	private ListCounter<ArticleData> positive;
	private ListCounter<ArticleData> negative;
	private ListCounter<ArticleData> neutral;
	
	public SentimentData() {

	}

	public ListCounter<ArticleData> getPositive() {
		return positive;
	}

	public void setPositive(ListCounter<ArticleData> positive) {
		this.positive = positive;
	}

	public ListCounter<ArticleData> getNegative() {
		return negative;
	}

	public void setNegative(ListCounter<ArticleData> negative) {
		this.negative = negative;
	}

	public ListCounter<ArticleData> getNeutral() {
		return neutral;
	}

	public void setNeutral(ListCounter<ArticleData> neutral) {
		this.neutral = neutral;
	}
	
}
