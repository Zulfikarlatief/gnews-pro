package io.gnews.pro.core.repository.mongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.EmailReceiver;

public interface EmailReceiverRepository extends MongoRepository<EmailReceiver, String>{

	public List<EmailReceiver> findByActiveTrue();

	public EmailReceiver findByEmail(String email);
	
}
