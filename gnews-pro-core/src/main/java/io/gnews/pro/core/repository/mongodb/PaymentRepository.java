package io.gnews.pro.core.repository.mongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.Payment;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.model.mongodb.UserAccount;


public interface PaymentRepository extends MongoRepository<Payment, String>{

	public Payment findByProject(Project project);

	public Payment findByInvoice(String invoice);

	//@Query("select a from Payment a where a.project.user.agent = ?1")
	//public List<Payment> findByAgent(AgentAccount account);

	//@Query("select a from Payment a where a.project.user = ?1")
	//public List<Payment> findByUser(UserAccount account);

	public List<Payment> findByProjectUserAndProjectDeletedFalse(UserAccount account);
	
	public Payment findById(String id);
	
	public Payment findProjectById(String id);
}
