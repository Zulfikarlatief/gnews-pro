package io.gnews.pro.core.model.dto;

/**
 * @author masasdani
 * Created Date Nov 3, 2015
 */
public class FilterCounter {

	private Long posts;
	private Long contributors;
	private Long exposure;
	private Long reach;
	
	
	public Long getPosts() {
		return posts;
	}
	
	public Long getContributors() {
		return contributors;
	}
	
	public Long getExposure() {
		return exposure;
	}
	
	public Long getReach() {
		return reach;
	}
	
	public void setPosts(Long posts) {
		this.posts = posts;
	}
	
	public void setContributors(Long contributors) {
		this.contributors = contributors;
	}
	
	public void setExposure(Long exposure) {
		this.exposure = exposure;
	}
	
	public void setReach(Long reach) {
		this.reach = reach;
	}
	
	
	
}
