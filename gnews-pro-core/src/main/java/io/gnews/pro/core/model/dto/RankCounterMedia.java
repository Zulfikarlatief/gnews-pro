package io.gnews.pro.core.model.dto;

import org.joda.time.DateTime;

public class RankCounterMedia {

	private String domain;
	private int value;
	private int globalRank;
	private DateTime date;
	private String countryName;
	private int countryRank;
	
	public RankCounterMedia() {
	
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public int getGlobalRank() {
		return globalRank;
	}

	public void setGlobalRank(int globalRank) {
		this.globalRank = globalRank;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public int getCountryRank() {
		return countryRank;
	}

	public void setCountryRank(int countryRank) {
		this.countryRank = countryRank;
	}
	
}
