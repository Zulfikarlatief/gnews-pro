package io.gnews.pro.core.model.mongodb;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@SuppressWarnings("serial")
@Document(collection = "email_group")
public class EmailGroup implements Serializable {

	@Id
	private String id;
	private String name;
	private String description;
	private Date createdAt;
	@DBRef
	private EmailReceiver emailReceiver;
	private EmailGroup parentGroup;
	private String projectId;
	private int memberCount = 0;
	
	public EmailGroup() {
		if(createdAt == null) createdAt = new Date();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public EmailReceiver getEmailReceiver() {
		return emailReceiver;
	}

	public void setEmailReceiver(EmailReceiver emailReceiver) {
		this.emailReceiver = emailReceiver;
	}
	
	public EmailGroup getParentGroup() {
		return parentGroup;
	}

	public void setParentGroup(EmailGroup parentGroup) {
		this.parentGroup = parentGroup;
	}

	public int getMemberCount() {
		return memberCount;
	}

	public void setMemberCount(int memberCount) {
		this.memberCount = memberCount;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

}
