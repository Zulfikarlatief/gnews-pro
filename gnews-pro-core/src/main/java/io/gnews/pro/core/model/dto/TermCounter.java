package io.gnews.pro.core.model.dto;

import org.springframework.data.annotation.Id;

/**
 * @author masasdani
 * Created Date Nov 3, 2015
 */
public class TermCounter implements Comparable<TermCounter> {

	@Id
	private String term;
	private int value;
	
	public TermCounter() {
	}

	public TermCounter(String term, int count) {
		super();
		this.term = term;
		this.value = count;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int compareTo(TermCounter o) {
		return o.getValue() - this.value;
	}
	
}
