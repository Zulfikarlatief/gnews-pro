package io.gnews.pro.core.repository.mongodb;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.Billing;


public interface BillingRepository extends MongoRepository<Billing, String>{
	
}
