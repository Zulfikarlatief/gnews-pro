package io.gnews.pro.core.util;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * helper class to do url cleaner fot 
 * @author masasdani
 * Created Date Oct 27, 2015
 */
public class URLCleanerUtil {

	/**
	 * @param url
	 * @return
	 */
	public static String getCleanUrl(String url){
		String validUrl = null;
		try {
			URI uri = new URI(url);
			uri = uri.normalize();
			if(isValidScheme(uri.getScheme())){
				if(isValidPath(uri.getPath())){
					validUrl = uri.getScheme()+"://"+uri.getHost()+uri.getPath();
					if(uri.getPath().split("/").length < 2){
						String param = uri.getQuery();
						validUrl = validUrl+"?"+param;
					}
				}
			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return validUrl;
	}
	
	/**
	 * @param scheme
	 * @return
	 */
	public static boolean isValidScheme(String scheme){
		if(scheme.equals("http") || scheme.equals("https")){
			return true;
		}
		return false;
	}
	
	/**
	 * @param path
	 * @return
	 */
	public static boolean isValidPath(String path){
		if(path != null){
			if(path.length() > 2){
				return true;
			}
		}
		return false;
	}
	
}
