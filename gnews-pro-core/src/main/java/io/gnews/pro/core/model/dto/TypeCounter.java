package io.gnews.pro.core.model.dto;

/**
 * @author masasdani
 * Created Date Nov 3, 2015
 */
public class TypeCounter {

	int id;
	Long value;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Long getValue() {
		return value;
	}
	
	public void setValue(Long value) {
		this.value = value;
	}
	
}
