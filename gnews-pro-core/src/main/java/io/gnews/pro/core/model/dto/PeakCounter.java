package io.gnews.pro.core.model.dto;

/**
 * @author masasdani
 * Created Date Nov 3, 2015
 */
public class PeakCounter {

	private Long id;
	private int value;
	
	public PeakCounter() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
}
