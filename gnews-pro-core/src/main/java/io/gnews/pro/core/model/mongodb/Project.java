package io.gnews.pro.core.model.mongodb;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * class for storing user data in jpa
 * 
 * @author madqori Created Date Jan 28, 2016
 */
@SuppressWarnings("serial")
@Document(collection = "project")
public class Project implements Serializable {
	
	public static final String PROJECT_INACTIVE 	= "inactive";
	public static final String PROJECT_ACTIVE 		= "active";
	public static final String PROJECT_EXPIRED_SOON = "expired-soon";
	public static final String PROJECT_EXPIRED 		= "expired";
	
	@Id
	private String id;
	private String name;
	private String keyword;
	private String filter;
	private String image;
	private String status = PROJECT_INACTIVE;
	@DBRef
	private UserAccount user;
	private Date createdAt;
	private Date expiredAt; 
	private Date lastUpdate;
	private Set<String> languanges;
	private boolean recurrent = false;
	private boolean active = false;
	private boolean expired = false;
	private boolean deleted = false;
	private String day;
	private int hour = 0;
	private int letterHour=0;
	private String email;
	private String companyLogo;
	private String companyColor;
	private String reportType;
	private String crawlState;
	private String gnewsLetterGroupId;
	
	public Project() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public UserAccount getUser() {
		return user;
	}

	public void setUser(UserAccount user) {
		this.user = user;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getExpiredAt() {
		return expiredAt;
	}

	public void setExpiredAt(Date expiredAt) {
		this.expiredAt = expiredAt;
	}

	public Set<String> getLanguanges() {
		return languanges;
	}

	public void setLanguanges(Set<String> languanges) {
		this.languanges = languanges;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public boolean isRecurrent() {
		return recurrent;
	}

	public void setRecurrent(boolean recurrent) {
		this.recurrent = recurrent;
	}

	public String getEmail() {
		return email;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public int getLetterHour() {
		return letterHour;
	}

	public void setLetterHour(int letterHour) {
		this.letterHour = letterHour;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompanyLogo() {
		return companyLogo;
	}

	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}

	public String getCompanyColor() {
		return companyColor;
	}

	public void setCompanyColor(String companyColor) {
		this.companyColor = companyColor;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getCrawlState() {
		return crawlState;
	}

	public void setCrawlState(String crawlState) {
		this.crawlState = crawlState;
	}

	public String getGnewsLetterGroupId() {
		return gnewsLetterGroupId;
	}

	public void setGnewsLetterGroupId(String gnewsLetterGroupId) {
		this.gnewsLetterGroupId = gnewsLetterGroupId;
	}
	
}
