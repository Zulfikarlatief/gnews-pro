package io.gnews.pro.core.util;

import com.google.common.net.InternetDomainName;

/**
 * @author masasdani
 * Created Date Oct 27, 2015
 */
public class DomainUtil {

	/**
	 * @param host
	 * @return
	 */
	public static String getTopPrivateDomain(String host) {
		InternetDomainName domainName = InternetDomainName.from(host);
		return domainName.topPrivateDomain().toString();
	}

}
