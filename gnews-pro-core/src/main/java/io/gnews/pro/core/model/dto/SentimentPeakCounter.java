package io.gnews.pro.core.model.dto;

import org.springframework.data.annotation.Id;

public class SentimentPeakCounter{

	@Id
	private Long peakDay;
	private int count;
 
	public SentimentPeakCounter() {
		// TODO Auto-generated constructor stub
	}
	public SentimentPeakCounter(Long peakDay,int count) {
		super();
		this.peakDay = peakDay;
		this.count = count;
	}
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Long getPeakDay() {
		return peakDay;
	}
	
	public void setPeakDay(Long peakDay) {
		this.peakDay = peakDay;
	}
	
}
