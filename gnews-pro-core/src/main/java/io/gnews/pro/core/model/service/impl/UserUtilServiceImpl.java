
package io.gnews.pro.core.model.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.model.mongodb.AgentAccount;
import io.gnews.pro.core.model.mongodb.UserAccount;
import io.gnews.pro.core.model.service.UserUtilService;
import io.gnews.pro.core.repository.mongodb.AccountRepository;
import io.gnews.pro.core.repository.mongodb.AgentAccountRepository;
import io.gnews.pro.core.repository.mongodb.UserAccountRepository;

public class UserUtilServiceImpl implements UserUtilService{


	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private UserAccountRepository userAccountRepository;
	
	@Autowired
	private AgentAccountRepository agentAccountRepository;

	@Override
	public UserAccount findUserByEmail(String email) {
		Account account = accountRepository.findByEmail(email);
		return userAccountRepository.findByAccount(account);
	}

	@Override
	public AgentAccount findAgentByEmail(String email) {
		Account account = accountRepository.findByEmail(email);
		return agentAccountRepository.findByAccount(account);
	}
}
