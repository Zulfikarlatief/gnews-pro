package io.gnews.pro.core.util;

import java.util.Random;

public class RandomUtil {

	public static String getRandomWord(int length) {
		String r = "";
		for (int i = 0; i < length; i++) {
			r += (char) (Math.random() * 26 + 97);
		}
		return r;
	}

	public static int getRandomNum() {

		Random randomGenerator = new Random();
		int t = randomGenerator.nextInt(100);
		if (t < 10) {
			t = randomGenerator.nextInt(100);
		}
		return t;
	}
	public static int getRandomNum(int n) {

		Random randomGenerator = new Random();
		int t = randomGenerator.nextInt(n);
		if (t < 1000) {
			getRandomNum(n);
		}
		return t;
	}

}
