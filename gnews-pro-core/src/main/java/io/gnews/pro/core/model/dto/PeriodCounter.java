package io.gnews.pro.core.model.dto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author masasdani
 * Created Date Nov 3, 2015
 */
public class PeriodCounter implements Comparable<PeriodCounter> {

	private String day;
	private List<TimeCounter> counts; 
	private int count;
	private TimeCounter peakTime;
	
	public PeriodCounter() {
		
	}

	public PeriodCounter(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		this.day =  simpleDateFormat.format(date);
		counts = new ArrayList<TimeCounter>();	
		for(int j=0;j<24;j++){
			TimeCounter timeCounter = new TimeCounter();
			timeCounter.setTime(j+"");
			timeCounter.setCount(0);
			counts.add(timeCounter);
		}
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public List<TimeCounter> getCounts() {
		return counts;
	}

	public void setCounts(List<TimeCounter> counts) {
		this.counts = counts;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public TimeCounter getPeakTime() {
		return peakTime;
	}

	public void setPeakTime(TimeCounter peakTime) {
		this.peakTime = peakTime;
	}

	public int compareTo(PeriodCounter o) {
		return o.getCount()-getCount();
	}
	
}
