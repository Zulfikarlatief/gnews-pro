package io.gnews.pro.core.config;

import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * config class for jpa (mysql)
 * 
 * @author masasdani
 * Created Date Oct 27, 2015
 */
@Configuration
@EntityScan(basePackages = {"io.gnews.pro.core"})
@EnableJpaRepositories(basePackages = {"io.gnews.pro.core"})
public class JpaConfig {

}
