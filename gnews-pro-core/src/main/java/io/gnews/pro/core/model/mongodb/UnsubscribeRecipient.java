package io.gnews.pro.core.model.mongodb;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import io.gnews.pro.core.config.CascadeSave;

@SuppressWarnings("serial")
@Document(collection = "unsubscribe_recipient")
public class UnsubscribeRecipient implements Serializable {

	@Id
	private String id;
	@DBRef
	@CascadeSave
	private GroupRecipient group;
	private Date accessDate;
	private String messageId;
	
	public UnsubscribeRecipient() {
	
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public GroupRecipient getGroup() {
		return group;
	}

	public void setGroup(GroupRecipient group) {
		this.group = group;
	}

	public Date getAccessDate() {
		return accessDate;
	}

	public void setAccessDate(Date accessDate) {
		this.accessDate = accessDate;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	
}
