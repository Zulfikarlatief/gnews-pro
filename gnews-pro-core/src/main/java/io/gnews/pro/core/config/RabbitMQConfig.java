package io.gnews.pro.core.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

@Configuration
@EnableRabbit
public class RabbitMQConfig implements RabbitListenerConfigurer {

	public static final String CREATE_PROJECT_QUEUE = "gnews.analytic.project.create";
	public static final String SEND_MAIL_QUEUE = "gnews.pro.mail.send";
	public static final String SEND_MAIL_AR = "gnews.pro.ar.mail.send";
	public static final String EMERGENCY_UPDATE_ALL_KM = "gnews.emergency.keywordmapping";
	public static final String EMERGENCY_UPDATE_KM = "gnews.emergency.spesifickeyword";
	public static final String EMERGENCY_UPDATE_ALL_PROJECT = "gnews.emergency.all.project";
	public static final String SAVE_MAIL_BLAST_QUEUE = "gnews.mail.blast.save";
	public static final String DISTRIBUTE_MAIL_BLAST_QUEUE = "gnews.mail.blast.distribute";
	public static final String DISTRIBUTE_MAIL_SEND_QUEUE = "gnews.mail.blast.send";
	public static final String GENERATE_WEEKLY_AR = "gnews.pro.ar.generate.weekly";
	public static final String EMERGENCY_PROJECT = "gnews.pro.emergency";
	
	@Value("${rabbitmq.auth.host}")
	private String rabbitmqHost;
	@Value("${rabbitmq.auth.username}")
	private String rabbitmqUsername;
	@Value("${rabbitmq.auth.password}")
	private String rabbitmqPassword;
	@Value("${rabbitmq.auth.port}")
	private int rabbitmqPort;
	
	@Bean
	public ConnectionFactory connectionFactory() {
	    CachingConnectionFactory connectionFactory = new CachingConnectionFactory(rabbitmqHost);
	    connectionFactory.setUsername(rabbitmqUsername);
	    connectionFactory.setPassword(rabbitmqPassword);
	    connectionFactory.setPort(rabbitmqPort);
	    connectionFactory.setVirtualHost("/");
	    return connectionFactory;
	}
	
    @Bean
    public RabbitAdmin rabbitAdmin(){
    	RabbitAdmin rabbitAdmin = new RabbitAdmin(connectionFactory());
    	rabbitAdmin.declareQueue(new Queue(SEND_MAIL_QUEUE, true));
    	rabbitAdmin.declareQueue(new Queue(DISTRIBUTE_MAIL_BLAST_QUEUE, true));
    	rabbitAdmin.declareQueue(new Queue(DISTRIBUTE_MAIL_SEND_QUEUE, true));
    	rabbitAdmin.declareQueue(new Queue(CREATE_PROJECT_QUEUE, true));
    	rabbitAdmin.declareQueue(new Queue(SAVE_MAIL_BLAST_QUEUE, true));
    	rabbitAdmin.declareQueue(new Queue(EMERGENCY_UPDATE_KM, true));
    	rabbitAdmin.declareQueue(new Queue(SEND_MAIL_AR,true));
    	rabbitAdmin.declareQueue(new Queue(GENERATE_WEEKLY_AR, true));
    	rabbitAdmin.declareQueue(new Queue(EMERGENCY_UPDATE_ALL_KM,true));
    	rabbitAdmin.declareQueue(new Queue(EMERGENCY_UPDATE_ALL_PROJECT, true));
    	rabbitAdmin.declareQueue(new Queue(EMERGENCY_PROJECT,true));
    	return rabbitAdmin;
    }
    
	@Bean
	public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory() {
	    SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
	    factory.setConnectionFactory(connectionFactory());
	    factory.setConcurrentConsumers(50);
	    factory.setMaxConcurrentConsumers(100);
	    return factory;
	}
	
	@Override
	public void configureRabbitListeners(RabbitListenerEndpointRegistrar registrar) {
		registrar.setMessageHandlerMethodFactory(myHandlerMethodFactory());
	}

	@Bean
	public DefaultMessageHandlerMethodFactory myHandlerMethodFactory() {
	    DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
	    factory.setMessageConverter(new MappingJackson2MessageConverter());
	    return factory;
	}
	
    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
    
    @Bean
    RabbitTemplate rabbitTemplate(){
    	RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory());
    	rabbitTemplate.setMessageConverter(jsonMessageConverter());
    	return rabbitTemplate;
    }

}
