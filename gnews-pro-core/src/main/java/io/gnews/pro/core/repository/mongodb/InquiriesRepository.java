package io.gnews.pro.core.repository.mongodb;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.Inquiries;

public interface InquiriesRepository extends MongoRepository<Inquiries, String>{
	
}
