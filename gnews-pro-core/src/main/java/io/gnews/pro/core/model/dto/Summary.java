package io.gnews.pro.core.model.dto;

import java.util.Date;
import java.util.List;

/**
 * @author masasdani
 * Created Date Nov 3, 2015
 */
public class Summary {

	private String keyword;
	private String filter;
	private int totalMedia;
	private int totalArticle;
	private List<TermCounter2> mostMedia;
	private List<PeriodCounter> peakTime;
	private List<TermCounter2> keywordMapping;
	private List<TermCounter2> keywordMappingPositive;
	private List<TermCounter2> keywordMappingNegative;
	private ListCounter<ArticleSimple> negative;
	private ListCounter<ArticleSimple> positive;
	private ListCounter<ArticleSimple> neutral;
	private Date startDate;
	private Date endDate;
	
	public Summary() {
		
	}

	public int getTotalMedia() {
		return totalMedia;
	}

	public void setTotalMedia(int totalMedia) {
		this.totalMedia = totalMedia;
	}

	public int getTotalArticle() {
		return totalArticle;
	}

	public void setTotalArticle(int totalArticle) {
		this.totalArticle = totalArticle;
	}

	public List<TermCounter2> getMostMedia() {
		return mostMedia;
	}

	public void setMostMedia(List<TermCounter2> mostMedia) {
		this.mostMedia = mostMedia;
	}

	public List<PeriodCounter> getPeakTime() {
		return peakTime;
	}

	public void setPeakTime(List<PeriodCounter> peakTime) {
		this.peakTime = peakTime;
	}

	public List<TermCounter2> getKeywordMapping() {
		return keywordMapping;
	}

	public void setKeywordMapping(List<TermCounter2> keywordMapping) {
		this.keywordMapping = keywordMapping;
	}

	public ListCounter<ArticleSimple> getNegative() {
		return negative;
	}

	public void setNegative(ListCounter<ArticleSimple> negative) {
		this.negative = negative;
	}

	public ListCounter<ArticleSimple> getPositive() {
		return positive;
	}

	public void setPositive(ListCounter<ArticleSimple> positive) {
		this.positive = positive;
	}

	public ListCounter<ArticleSimple> getNeutral() {
		return neutral;
	}

	public void setNeutral(ListCounter<ArticleSimple> neutral) {
		this.neutral = neutral;
	}

	public List<TermCounter2> getKeywordMappingPositive() {
		return keywordMappingPositive;
	}

	public void setKeywordMappingPositive(List<TermCounter2> keywordMappingPositive) {
		this.keywordMappingPositive = keywordMappingPositive;
	}

	public List<TermCounter2> getKeywordMappingNegative() {
		return keywordMappingNegative;
	}

	public void setKeywordMappingNegative(List<TermCounter2> keywordMappingNegative) {
		this.keywordMappingNegative = keywordMappingNegative;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}
	
}
