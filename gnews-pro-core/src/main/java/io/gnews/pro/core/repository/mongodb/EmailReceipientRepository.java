package io.gnews.pro.core.repository.mongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.EmailRecipient;

public interface EmailReceipientRepository extends MongoRepository<EmailRecipient, String> {

	public EmailRecipient findByEmail(String email);

	public List<EmailRecipient> findByEmailIn(List<String> emails);

}
