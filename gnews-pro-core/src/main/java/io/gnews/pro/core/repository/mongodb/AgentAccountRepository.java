package io.gnews.pro.core.repository.mongodb;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.model.mongodb.AgentAccount;

public interface AgentAccountRepository extends MongoRepository<AgentAccount, String>{

	public AgentAccount findByAccount(Account accounts);
	
	public AgentAccount findByAccountAndAccountActiveTrue(String email);

}
