package io.gnews.pro.core.repository.mongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.Account;


/**
 * @author masasdani
 * Created Date Oct 27, 2015
 */
public interface AccountRepository extends MongoRepository<Account, String> {

	/**
	 * @param email
	 * @return
	 */
	public Account findByEmailAndActiveTrue(String email);

	/**
	 * @param email
	 * @return
	 */
	public Account findByEmail(String email);
	
	/**
	 * @param role
	 * @return
	 */
	public List<Account> findByType(String type);
	
	/**
	 * @return
	 */
	public List<Account> findByActiveTrue();
	
	/**
	 * @param type
	 * @return
	 */
	public Long countByType(String type);
	
	
	
}
