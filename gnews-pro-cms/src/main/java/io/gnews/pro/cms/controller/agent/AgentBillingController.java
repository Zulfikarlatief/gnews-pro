package io.gnews.pro.cms.controller.agent;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.gnews.pro.cms.model.response.BillingData;
import io.gnews.pro.cms.service.BillingService;
import io.gnews.pro.cms.service.SiteService;
import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.repository.mongodb.AccountRepository;

@Controller
@RequestMapping(value="/agent/billing")
public class AgentBillingController {
	
	@Autowired
	SiteService siteService;
	
	@Autowired
	BillingService billingService;
	
	@Autowired
	AccountRepository accountRepository;
	
	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public String index(Principal principal, Model model) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		List<BillingData> billing= billingService.getBillingForAgent(principal);
		model.addAttribute("billing", billing);

		return "agent/billing";
	}
}
