package io.gnews.pro.cms.service;

import java.security.Principal;

import io.gnews.pro.cms.model.request.ChangePasswordForm;
import io.gnews.pro.cms.model.request.DefaultPriceForm;
import io.gnews.pro.cms.model.response.ProfileData;;

public interface SettingService {

	/**
	 * @param id
	 * @return
	 */
	public ProfileData getProfileById(String id);

	/**
	 * @param name
	 * @return
	 */
	public ProfileData getProfileByEmail(String email);

	/**
	 * @param form
	 * @param principal
	 * @throws Exception
	 */
	public void save(ChangePasswordForm form, Principal principal) throws Exception;
	
	/**
	 * @param price
	 * @param principal
	 */
	public void savePrice(DefaultPriceForm form, Principal principal);

}
