package io.gnews.pro.cms.service.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gnews.pro.cms.service.PromoCodeSchedulerService;
import io.gnews.pro.core.model.mongodb.PromoCode;
import io.gnews.pro.core.repository.mongodb.PromoCodeRepository;

@Service
public class PromoCodeSchedulerImpl implements PromoCodeSchedulerService{

	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	PromoCodeRepository promoCodeRepository;
	
	@Override
	public void updateAllData(Date date) {	
		
		log.info("update all Article By Data");
		List<PromoCode> list = promoCodeRepository.findCodeByExpiredAt(date);
		for(PromoCode promoCode : list){
				log.info("Checked for PromoCode expired Date");
				promoCode.setExpired(true);
				promoCodeRepository.save(promoCode);
			}
		}
	}

