package io.gnews.pro.cms.service;


import java.security.Principal;
import java.util.List;

import org.springframework.stereotype.Service;

import io.gnews.pro.cms.model.request.AgentForm;
import io.gnews.pro.cms.model.response.AgentData;

@Service
public interface AgentService {

	/**
	 * @param principal
	 * @return
	 */
	public List<AgentData> getAgentData(Principal principal);
	
	/**
	 * @param id
	 * @return
	 */
	public AgentData getAgentById(String id);
	
	/**
	 * @param id
	 */
	public void delete(String id);

	/**
	 * @param principal
	 * @param form
	 */
	void save(Principal principal, AgentForm form);
}
