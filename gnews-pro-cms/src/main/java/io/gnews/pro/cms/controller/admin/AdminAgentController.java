package io.gnews.pro.cms.controller.admin;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import io.gnews.pro.cms.model.request.AgentForm;
import io.gnews.pro.cms.model.response.AgentData;
import io.gnews.pro.cms.service.AgentService;
import io.gnews.pro.cms.service.SiteService;
import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.repository.mongodb.AccountRepository;

@Controller
@RequestMapping(value = "/admin/agent")
public class AdminAgentController {

	@Autowired
	SiteService siteService;

	@Autowired
	AgentService agentService;
	
	@Autowired
	AccountRepository accountRepository;

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public String index(Principal principal, Model model) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		List<AgentData> agent = agentService.getAgentData(principal);
		model.addAttribute("agent", agent);

		return "admin/all-agent";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/add")
	public String add(Principal principal, Model model) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		model.addAttribute("agent", new AgentData());

		return "admin/new-agent";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/edit")
	public String edit(Principal principal, Model model, @RequestParam String id) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		AgentData agent = agentService.getAgentById(id);
		model.addAttribute("agent", agent);
		
		return "admin/new-agent";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/delete")
	public String delete(Principal principal, Model model, @RequestParam String id) {
		if (principal == null) {
			return "redirect:/signin";
		}
		agentService.delete(id);
		
		return "redirect:/admin/agent/all";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save")
	public String add(Principal principal, @ModelAttribute AgentForm Form) {
		if (principal == null) {
			return "redirect:/signin";
		}
		agentService.save(principal, Form);
		
		return "redirect:/admin/agent/all";
	}
}
