package io.gnews.pro.cms.model.request;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class PromoCodeForm {
	
	public String id;
	public String codename;
	public Long price;
	public int period;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	public Date expiredDate;
	public String indentifyAgent;
	
	public PromoCodeForm() {
		// TODO Auto-generated constructor stub
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCodename() {
		return codename;
	}
	
	public void setCodename(String codename) {
		this.codename = codename;
	}
	
	public Long getPrice() {
		return price;
	}
	
	public void setPrice(Long price) {
		this.price = price;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public String getIndentifyAgent() {
		return indentifyAgent;
	}

	public void setIndentifyAgent(String indentifyAgent) {
		this.indentifyAgent = indentifyAgent;
	}
}
