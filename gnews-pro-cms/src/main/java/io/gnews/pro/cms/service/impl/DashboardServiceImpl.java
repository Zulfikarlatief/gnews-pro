package io.gnews.pro.cms.service.impl;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gnews.pro.cms.service.DasboardService;
import io.gnews.pro.core.repository.mongodb.AccountRepository;
import io.gnews.pro.core.repository.mongodb.AgentAccountRepository;
import io.gnews.pro.core.repository.mongodb.ProjectRepository;
import io.gnews.pro.core.repository.mongodb.PromoCodeRepository;
import io.gnews.pro.core.repository.mongodb.UserAccountRepository;

@Service
public class DashboardServiceImpl implements DasboardService{

	Logger log = LoggerFactory.getLogger(getClass());	

	@Autowired
	UserAccountRepository userAccountRepository;
	
	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	AgentAccountRepository agentAccountRepository;
	
	@Autowired
	PromoCodeRepository promoCodeRepository;
	
	@Autowired
	ProjectRepository projectRepository;
	
	@Override
	public Long countRecurrent(Principal principal) {	
		/*AgentAccount agentAccount = agentAccountRepository.findByAccountEmail(principal.getName());
		return userAccountRepository.countByAcccountAgent(agentAccount);
		*/
		return null;
	}
	
	@Override
	public Long countCodeGenerated(Principal principal) {	
	/*	Account account = accountRepository.findByEmail(principal.getName());
		log.info(account.getEmail());
		return promoCodeRepository.countByAgent(account);*/
		return null;
	}
	
	@Override
	public int countCodeSubmited(Principal principal) {	
		/*int a=0;
		AgentAccount agentAccount = agentAccountRepository.findByAccountEmail(principal.getName());
		List<PromoCode> listSubmitted = promoCodeRepository.findCodeByAgentAndSubmited(agentAccount, true);
		for (int i = 0; i < listSubmitted.size(); i++) {
			a++;
		}
		return a;*/
		return 0;
	}
	
	@Override
	public int countCodeExpired(Principal principal){
	/*	int a=0;
		AgentAccount agentAccount = agentAccountRepository.findByAccountEmail(principal.getName());
		List<PromoCode> listExpired = promoCodeRepository.findCodeByAgentAndExpired(agentAccount, true);
		for (int i = 0; i < listExpired.size(); i++) {
			a++;
		}
		return a;*/
		return 0;
	}
	
	@Override
	public int countClientproject(Principal principal){
		return 0;
		/*int a = 0;
		AgentAccount agentAccount = agentAccountRepository.findByAccountEmail(principal.getName());
		List<Project> listProject= projectRepository.findByUserAgentAndActive(agentAccount, true);
		
		for (int i = 0; i < listProject.size(); i++) {
			a++;
		}
		return a;*/
	}
	
}
