package io.gnews.pro.cms.model.response;

import io.gnews.pro.core.model.mongodb.Account;

public class UserData {
	
	public String accountId;
	public String userId;
	public String email;
	public String fullname;
	public String company;
	public String address;
	public Account agent;
	public String password;
	public boolean active;
	
	
	public UserData() {
		// TODO Auto-generated constructor stub
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}

	public Account getAgent() {
		return agent;
	}

	public void setAgent(Account agent) {
		this.agent = agent;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
