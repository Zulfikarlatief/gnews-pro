package io.gnews.pro.cms.service;

import java.security.Principal;
import java.util.List;

import org.springframework.stereotype.Service;

import io.gnews.pro.cms.model.request.PromoCodeForm;
import io.gnews.pro.cms.model.response.PromoCodeData;

@Service
public interface PromoCodeService {

	/**
	 * @param principal
	 * @return
	 */
	public List<PromoCodeData> getCreateCodeData(Principal principal);
	
	/**
	 * @param principal
	 * @return
	 */
	public String GetCodename(Principal principal);
	
	/**
	 * @param principal
	 * @param form
	 */
	public void save(Principal principal,PromoCodeForm form);
	
	/**
	 * @param id
	 * @return
	 */
	public PromoCodeData getCreateCodeById(String id);
	
	/**
	 * @param id
	 */
	public void delete(String id);
}
