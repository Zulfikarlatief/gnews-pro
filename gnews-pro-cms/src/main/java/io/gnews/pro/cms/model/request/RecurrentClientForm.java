package io.gnews.pro.cms.model.request;

public class RecurrentClientForm {
	
	public String id;
	public String email;
	public String fullname;
	public String company;
	public String address;
	public String password;
	public String identifyAgent;
	
	public RecurrentClientForm() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getIdentifyAgent() {
		return identifyAgent;
	}

	public void setIdentifyAgent(String identifyAgent) {
		this.identifyAgent = identifyAgent;
	}
}
