package io.gnews.pro.cms.service.impl;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import io.gnews.pro.cms.model.request.AgentForm;
import io.gnews.pro.cms.model.response.AgentData;
import io.gnews.pro.cms.service.AgentService;
import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.model.mongodb.AgentAccount;
import io.gnews.pro.core.repository.mongodb.AgentAccountRepository;

@Service
public class AgentServiceImpl implements AgentService{
	
	@Autowired
	private AgentAccountRepository agentAccountRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public void save(Principal principal, AgentForm form) {
		AgentAccount agent = new AgentAccount();
		if (form.getId() != null && form.getId().length()>0)
			agent.setId(form.getId());
		
		agent.getAccount().setEmail(form.getEmail());
		agent.setFullname(form.getFullname());
		agent.setCompany(form.getCompany());
		agent.setAddress(form.getAddress());
		agent.setCodename(form.getCodename());
		agent.getAccount().setPassword(passwordEncoder.encode(form.getPassword()));
		agent.getAccount().setCreatedAt(new Date());
		agent.getAccount().setActive(true);
		agent.getAccount().setType(Account.TYPE_AGENT);

		agentAccountRepository.save(agent);
	}
	
	@Override
	public List<AgentData> getAgentData(Principal principal) {
		List<AgentData> agentDatas = new ArrayList<>();
		List<AgentAccount> list = agentAccountRepository.findAll();
		
		for(AgentAccount agent : list){
			AgentData agentData = new AgentData();
			if(agent.getAccount().getType().equals(Account.TYPE_AGENT)){
				agentData.setId(agent.getId());
				agentData.setEmail(agent.getAccount().getEmail());
				agentData.setFullname(agent.getFullname());
				agentData.setCompany(agent.getCompany());
				agentData.setAddress(agent.getAddress());
				agentData.setCodename(agent.getCodename() );
				agentDatas.add(agentData);
			}
			continue;
		}
		return agentDatas;
	}

	@Override
	public AgentData getAgentById(String id) {
		AgentAccount agent = agentAccountRepository.findOne(id);
		if (agent == null){
			return null;
		}
		AgentData agentData = new AgentData();
		agentData.setId(agent.getId());
		agentData.setEmail(agent.getAccount().getEmail());
		agentData.setFullname(agent.getFullname());
		agentData.setCompany(agent.getCompany());
		agentData.setAddress(agent.getAddress());
		agentData.setCodename(agent.getCodename());
		return agentData;
	}
	
	@Override
	public void delete(String id){
		agentAccountRepository.delete(id);
	}	
}
