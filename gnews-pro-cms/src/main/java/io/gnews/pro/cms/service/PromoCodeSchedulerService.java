package io.gnews.pro.cms.service;

import java.util.Date;

/**
 * @author masasdani
 * Created Date Nov 9, 2015
 */
public interface PromoCodeSchedulerService {

	public void updateAllData(Date date);
	
}
