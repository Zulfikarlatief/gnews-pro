package io.gnews.pro.cms.controller.admin;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import io.gnews.pro.cms.model.request.BillingForm;
import io.gnews.pro.cms.model.response.BillingData;
import io.gnews.pro.cms.service.BillingService;
import io.gnews.pro.cms.service.SiteService;
import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.repository.mongodb.AccountRepository;

@Controller
@RequestMapping(value = "/admin/billing")
public class AdminBillingController {

	@Autowired
	SiteService siteService;

	@Autowired
	BillingService billingService;
	
	@Autowired
	AccountRepository accountRepository;

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public String index(Principal principal, Model model) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		List<BillingData> billing= billingService.getBillingData(principal);
		model.addAttribute("billing", billing);

		return "admin/all-billing";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/add")
	public String add(Principal principal, Model model) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		model.addAttribute("agentData", billingService.getAllAgent());
		model.addAttribute("billing", new BillingData());

		return "admin/new-billing";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/edit")
	public String edit(Principal principal, Model model, @RequestParam String id) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		BillingData billing = billingService.getBillingById(id);
		model.addAttribute("billing", billing);

		return "admin/new-billing";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/delete")
	public String delete(Principal principal, Model model, @RequestParam String id) {
		if (principal == null) {
			return "redirect:/signin";
		}
		billingService.delete(id);
		
		return "redirect:/admin/billing/all";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save")
	public String add(Principal principal, @ModelAttribute BillingForm Form) {
		if (principal == null) {
			return "redirect:/signin";
		}
		billingService.save(principal, Form);
		
		return "redirect:/admin/billing/all";
	}
}
