package io.gnews.pro.cms.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.gnews.pro.cms.service.SiteService;
import io.gnews.pro.core.repository.mongodb.AccountRepository;

@Controller
@RequestMapping(value = "/")
public class DefaultController {

	@Autowired
	SiteService siteService;
	
	@Autowired
	AccountRepository accountRepository;
	
	@RequestMapping(method = RequestMethod.GET)
	public String index(Principal principal,Model model){
		return signin(principal,model);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "signin")
	public String signin(Principal principal, Model model){
		siteService.setSiteInfo(principal,model);
		if (principal == null)
			return "signin";
		return "redirect:/home";
				
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "signup")
	public String signup(Principal principal){
		if (principal != null)
			return "redirect:home/";
		return "signup";
	}

	@RequestMapping(method = RequestMethod.GET, value = "reset-password")
	public String resetPassword(Principal principal){
		return "reset-password";
	}
	
}
