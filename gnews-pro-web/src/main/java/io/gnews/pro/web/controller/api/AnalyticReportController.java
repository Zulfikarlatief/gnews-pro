package io.gnews.pro.web.controller.api;

import io.swagger.annotations.Api;

import java.security.Principal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.gnews.pro.core.model.dto.ArticleData;
import io.gnews.pro.core.model.dto.PeakCounter;
import io.gnews.pro.core.model.dto.RankCounterMedia;
import io.gnews.pro.core.model.dto.SentimentCounter;
import io.gnews.pro.core.model.dto.SentimentPeakCounter;
import io.gnews.pro.core.model.dto.TermCounter;
import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.util.DateUtils;
import io.gnews.pro.web.repository.mongodb.WebDataAnalyticRepository;
import io.gnews.pro.web.response.DefaultResponse;
import io.gnews.pro.web.response.ResponseData;
import io.gnews.pro.web.service.AccountService;
import io.gnews.pro.web.service.ProjectService;

/**
 * @author masasdani
 * Created Date Nov 24, 2015
 */
@Api(value="Analytic Report", tags="Analytic Report API")
@RestController
@RequestMapping("/api/report")
public class AnalyticReportController {

	private static final int DEFAULT_LIMIT_DATA = 20;

	private static final int DEFAULT_ANALYTIC_PERIOD_DAY = 7;
	
	@Autowired
	private WebDataAnalyticRepository analyticRepository;

	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private AccountService accountService;
	
	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/media-count", method = RequestMethod.GET)
	public Object getMediaCount(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			Long mediaCount = analyticRepository.getMediaCount(project, start, end);
			return new ResponseData<Long>(mediaCount);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/article-count", method = RequestMethod.GET)
	public Object getArticleCount(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			Long mediaCount = analyticRepository.getArticleCount(project, start, end);
			return new ResponseData<Long>(mediaCount);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/keyword-mapping", method = RequestMethod.GET)
	public Object getKeywordMapping(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<TermCounter> list = analyticRepository.getKeywordMappingNew(project, start, end);
			return new ResponseData<List<TermCounter>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/keyword-mapping/positive", method = RequestMethod.GET)
	public Object getKeywordMappingPositive(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<TermCounter> list = analyticRepository.getKeywordMappingPositive(project, start, end);
			return new ResponseData<List<TermCounter>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/keyword-mapping/negative", method = RequestMethod.GET)
	public Object getKeywordMappingNegative(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<TermCounter> list = analyticRepository.getKeywordMappingNegative(project, start, end);
			return new ResponseData<List<TermCounter>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param principal
	 * @param request
	 * @param id
	 * @param term
	 * @param accessToken
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/{id}/keyword-mapping/article", method = RequestMethod.GET)
	public Object getArticleByTerms(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
			@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String term,
    		@RequestParam(required=false) String accessToken,
    		@RequestParam(required=false) Integer offset,
    		@RequestParam(required=false) Integer limit
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			if(term == null){
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(offset == null) offset = 0;
			if(limit == null) limit = DEFAULT_LIMIT_DATA;			
			List<ArticleData> list = analyticRepository.getArticleByTerm(project, term, start, end, limit, offset);
			return new ResponseData<List<ArticleData>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}
	
	/**
	 * @param principal
	 * @param request
	 * @param id
	 * @param term
	 * @param start
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/keyword-mapping/articlecount", method = RequestMethod.GET)
	public Object getArticleCountByTerm(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
			@RequestParam(required=false) String term,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			Long articleCount = analyticRepository.getArticleCountByTerm(project,term, start, end);
			return new ResponseData<Long>(articleCount);	
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}
	
	@RequestMapping(value = "/{id}/keyword-mapping/mediacount", method = RequestMethod.GET)
	public Object getMediaCountByTerm(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
			@RequestParam(required=false) String term,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			Long articleCount = analyticRepository.getMediaCountByTerm(project, term, start, end);
			return new ResponseData<Long>(articleCount);	
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}
	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/{id}/most-media", method = RequestMethod.GET)
	public Object getMostMedia(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken,
    		@RequestParam(required=false) Integer offset,
    		@RequestParam(required=false) Integer limit
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(offset == null) offset = 0;
			if(limit == null) limit = DEFAULT_LIMIT_DATA;			
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<TermCounter> list = analyticRepository.getMostMedia(project, start, end, limit, offset);
			return new ResponseData<List<TermCounter>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}
	
	/**
	 * @param principal
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/{id}/most-media/article", method = RequestMethod.GET)
	public Object getMostMediaByTerm(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String term,
    		@RequestParam(required=false) String accessToken,
    		@RequestParam(required=false) Integer offset,
    		@RequestParam(required=false) Integer limit
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(offset == null) offset = 0;
			if(limit == null) limit = DEFAULT_LIMIT_DATA;			
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<ArticleData> list = analyticRepository.getMostMediaByDomain(project, term, start, end, limit, offset);
			return new ResponseData<List<ArticleData>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/{id}/most-media/positive", method = RequestMethod.GET)
	public Object getMostMediaPositive(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken,
    		@RequestParam(required=false) Integer offset,
    		@RequestParam(required=false) Integer limit
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(offset == null) offset = 0;
			if(limit == null) limit = DEFAULT_LIMIT_DATA;			
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<TermCounter> list = analyticRepository.getMostMediaPositive(project, start, end, limit, offset);
			return new ResponseData<List<TermCounter>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/{id}/most-media/negative", method = RequestMethod.GET)
	public Object getMostMediaNegative(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken,
    		@RequestParam(required=false) Integer offset,
    		@RequestParam(required=false) Integer limit
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(offset == null) offset = 0;
			if(limit == null) limit = DEFAULT_LIMIT_DATA;			
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<TermCounter> list = analyticRepository.getMostMediaNegative(project, start, end, limit, offset);
			return new ResponseData<List<TermCounter>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/{id}/most-media/neutral", method = RequestMethod.GET)
	public Object getMostMediaNeutral(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken,
    		@RequestParam(required=false) Integer offset,
    		@RequestParam(required=false) Integer limit
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(offset == null) offset = 0;
			if(limit == null) limit = DEFAULT_LIMIT_DATA;			
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<TermCounter> list = analyticRepository.getMostMediaNeutral(project, start, end, limit, offset);
			return new ResponseData<List<TermCounter>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param request
	 * @param id
	 * @param start
	 * 
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/most-media/popular", method = RequestMethod.GET)
	public Object getMostMediaPopular(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<RankCounterMedia> list = analyticRepository.getMostPopularMedia(project, start, end);
			return new ResponseData<List<RankCounterMedia>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}
	
	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/most-media/popular/positive", method = RequestMethod.GET)
	public Object getMostMediaPopularPositive(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken,
    		@RequestParam(required=false) int limit
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<RankCounterMedia> list = analyticRepository.getMostPopularPositiveMedia(project, start, end, limit);
			return new ResponseData<List<RankCounterMedia>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/most-media/popular/negative", method = RequestMethod.GET)
	public Object getMostMediaPopularNegative(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken,
    		@RequestParam(required=false) int limit
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<RankCounterMedia> list = analyticRepository.getMostPopularNegativeMedia(project, start, end,limit);
			return new ResponseData<List<RankCounterMedia>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/peak-day", method = RequestMethod.GET)
	public Object getPeakDay(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<PeakCounter> list = analyticRepository.getPeakDay(project, start, end);
			return new ResponseData<List<PeakCounter>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}
	
	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/peak-day/media", method = RequestMethod.GET)
	public Object getPeakDayMedia(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<PeakCounter> list = analyticRepository.getPeakDayMedia(project, start, end);
			return new ResponseData<List<PeakCounter>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}
	
	/**
	 * @param principal
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/peak-day/id", method = RequestMethod.GET)
	public Object getPeakDayById(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<ArticleData> list = analyticRepository.getPeakDayById(project, id, start, end);
			return new ResponseData<List<ArticleData>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/peak-hour", method = RequestMethod.GET)
	public Object getPeakHour(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<PeakCounter> list = analyticRepository.getPeakHour(project, start, end);
			return new ResponseData<List<PeakCounter>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param principal
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/peak-hour-media", method = RequestMethod.GET)
	public Object getPeakHourCount(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id,
			@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
			@RequestParam(required=false) Long peakHour,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			Long mediaCount = analyticRepository.getPeakHourMediaCount(project,start,end, peakHour);
			return new ResponseData<Long>(mediaCount);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}
	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/sentiment", method = RequestMethod.GET)
	public Object getSentiment(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			SentimentCounter counter = analyticRepository.getSentimentCounter(project, start, end);
			return new ResponseData<SentimentCounter>(counter);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}
	
	/**
	 * @param principal
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/{id}/sentiment/daypositive", method = RequestMethod.GET)
	public Object getSentimentPositiveEachDay(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<SentimentPeakCounter> counter = analyticRepository.getSentimentPositiveEachDay(project, start, end);
			return new ResponseData<List<SentimentPeakCounter>>(counter);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	@RequestMapping(value = "/{id}/sentiment/daynegative", method = RequestMethod.GET)
	public Object getSentimentNegativeEachDay(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<SentimentPeakCounter> counter = analyticRepository.getSentimentNegativeEachDay(project, start, end);
			return new ResponseData<List<SentimentPeakCounter>>(counter);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}
	
	@RequestMapping(value = "/{id}/sentiment/dayneutral", method = RequestMethod.GET)
	public Object getSentimentNeutralEachDay(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<SentimentPeakCounter> counter = analyticRepository.getSentimentNeutralEachDay(project, start, end);
			return new ResponseData<List<SentimentPeakCounter>>(counter);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}
	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/{id}/sentiment/positive", method = RequestMethod.GET)
	public Object getArticlePositive(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken,
    		@RequestParam(required=false) Integer offset,
    		@RequestParam(required=false) Integer limit
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(offset == null) offset = 0;
			if(limit == null) limit = DEFAULT_LIMIT_DATA;			
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<ArticleData> list = analyticRepository.getArticlePositive(project, start, end, limit, offset);
			return new ResponseData<List<ArticleData>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/{id}/sentiment/negative", method = RequestMethod.GET)
	public Object getArticleNegative(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken,
    		@RequestParam(required=false) Integer offset,
    		@RequestParam(required=false) Integer limit
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(offset == null) offset = 0;
			if(limit == null) limit = DEFAULT_LIMIT_DATA;			
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<ArticleData> list = analyticRepository.getArticleNegative(project, start, end, limit, offset);
			return new ResponseData<List<ArticleData>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param request
	 * @param id
	 * @param start
	 * @param end
	 * @param accessToken
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/{id}/sentiment/neutral", method = RequestMethod.GET)
	public Object getArticleNeutral(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
    		@RequestParam(required=false) String accessToken,
    		@RequestParam(required=false) Integer offset,
    		@RequestParam(required=false) Integer limit
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(offset == null) offset = 0;
			if(limit == null) limit = DEFAULT_LIMIT_DATA;			
			if(end == null) end = new Date();
			if(start == null) start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
			List<ArticleData> list = analyticRepository.getArticleNeutral(project, start, end, limit, offset);
			return new ResponseData<List<ArticleData>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param request
	 * @param id
	 * @param peakDay
	 * @param accessToken
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/{id}/peak-day/article", method = RequestMethod.GET)
	public Object getArticleByPeakDay(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) Long peakDay,
    		@RequestParam(required=false) String accessToken,
    		@RequestParam(required=false) String sentiment,
    		@RequestParam(required=false) Integer offset,
    		@RequestParam(required=false) Integer limit
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(offset == null) offset = 0;
			if(limit == null) limit = DEFAULT_LIMIT_DATA;			
			List<ArticleData> list = null;
			if(sentiment != null){
				list = analyticRepository.getArticleSentimentByPeakDay(project, peakDay, sentiment, limit, offset);
			}else{
				list = analyticRepository.getArticleByPeakDay(project, peakDay, limit, offset);
			}
			return new ResponseData<List<ArticleData>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}

	/**
	 * @param request
	 * @param id
	 * @param peakHour
	 * @param accessToken
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/{id}/peak-hour/article", method = RequestMethod.GET)
	public Object getArticleByPeakHour(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) Long peakHour,
    		@RequestParam(required=false) String accessToken,
    		@RequestParam(required=false) Integer offset,
    		@RequestParam(required=false) Integer limit
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			if(offset == null) offset = 0;
			if(limit == null) limit = DEFAULT_LIMIT_DATA;			
			List<ArticleData> list = analyticRepository.getArticleByPeakHour(project, peakHour, limit, offset);
			return new ResponseData<List<ArticleData>>(list);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}
	
	
	@RequestMapping(value = "/{id}/get-single/article", method = RequestMethod.GET)
	public Object getSingleArticleById(
			Principal principal,
			HttpServletRequest request,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) String accessToken,
    		@RequestParam(required=false) String urlId
    		) {
		try{
			Account account = null;
			if(principal == null){
				if(accessToken == null){
					return DefaultResponse.forbiddenError();
				}
				account = accountService.getAccountByToken(accessToken);
			}else{
				account = accountService.getAccountByEmail(principal.getName());
			}
			if(account == null) {
				return DefaultResponse.forbiddenError();
			}
			Project project = projectService.getById(id);
			if(project == null) {
				return DefaultResponse.forbiddenError();
			}
			ArticleData data = analyticRepository.getSingleArticleById(project, urlId);
			return new ResponseData<ArticleData>(data);
		}catch(Exception exception){
			return DefaultResponse.error(exception.getMessage());
		}
	}
}

	
