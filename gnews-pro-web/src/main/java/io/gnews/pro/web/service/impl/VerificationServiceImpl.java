package io.gnews.pro.web.service.impl;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gnews.pro.core.model.mongodb.EmailSender;
import io.gnews.pro.core.repository.mongodb.EmailSenderRepository;
import io.gnews.pro.web.service.VerificationService;
import io.gnews.pro.web.util.URLUtils;

@Service
public class VerificationServiceImpl implements VerificationService {

	@Autowired
	private EmailSenderRepository emailSenderRepository;
	
	@Override
	public boolean validateEmailSender(String token) {
		EmailSender emailSender = emailSenderRepository.findByValidationToken(token);
		if(emailSender != null){
			emailSender.setValid(true);
			emailSenderRepository.save(emailSender);
			return true;
		}
		return false;
	}

	@Override
	public String generateSenderVerificationUrl(EmailSender emailSender, HttpServletRequest request) {
		return URLUtils.getBaseURl(request) + "/verify/email/sender/" + getVerificationToken(emailSender);
	}

	private String getVerificationToken(EmailSender emailSender){
		String token = UUID.randomUUID().toString();
		if(emailSenderRepository.findByValidationToken(token) == null){
			emailSender.setValidationToken(token);
			emailSenderRepository.save(emailSender);
			return token;
		}else{
			return getVerificationToken(emailSender);
		}
	}
}
