package io.gnews.pro.web.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import io.gnews.pro.core.model.dto.RankCounterMedia;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.util.DateUtils;
import io.gnews.pro.web.config.AppConfig;
import io.gnews.pro.web.repository.mongodb.WebDataAnalyticRepository;
import io.gnews.pro.web.service.ProjectService;

@Controller
@RequestMapping(value = "/media")
public class MediaReputationController {

	private static final int DEFAULT_ANALYTIC_PERIOD_DAY = 7;
	
	private static final int MAX_DATA_RANKING = 8;
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@Autowired
	private WebDataAnalyticRepository analyticRepository;

	@Autowired
	private ProjectService projectService;
	
	@RequestMapping(value = "/{id}/media-reputation", method = RequestMethod.GET)
	public Object getArticleCount(Model model, HttpServletRequest request, 
			@PathVariable(value = "id") String id,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date end,
			@RequestParam(required = false) String accessToken,
			@RequestParam(required = false) Integer offset,
			@RequestParam(required = false) Integer limit) {
		try {
			if (accessToken.equals(AppConfig.STATIC_ACCESSTOKEN)) {
				Project project = projectService.getById(id);
				if (project == null) {
					return "analytics/media-reputation/blank-page";
				}
				if (end == null)
					end = new Date();
				if (start == null)
					start = DateUtils.before(end, DEFAULT_ANALYTIC_PERIOD_DAY);
				if (limit == null){
					limit = MAX_DATA_RANKING;
				}

				model.addAttribute("start", dateFormat.format(start));
				model.addAttribute("end", dateFormat.format(end));
				model.addAttribute("project", projectService.getById(id));
				
				// count article
				Long articleCount = analyticRepository.getArticleCount(project, start, end);
				System.out.printf("ini berapa sih" + articleCount);
				if(articleCount == 0L){
					return "analytics/media-reputation/blank-page";
				}
				model.addAttribute("articleCount", articleCount);

				// count media
				Long mediaCount = analyticRepository.getMediaCount(project, start, end);
				if(mediaCount == 0L){
					return "analytics/media-reputation/blank-page";
				}
				model.addAttribute("mediaCount", mediaCount);
				
				// data Popular
				List<RankCounterMedia> listPositive = analyticRepository.getMostPopularPositiveMedia(project, start, end,
						limit);
				List<RankCounterMedia> rankCountersPositive = new ArrayList<>();
				for (RankCounterMedia rankCounter : listPositive) {
					RankCounterMedia rc = new RankCounterMedia();
					rc.setCountryName(rankCounter.getCountryName());
					rc.setDomain(rankCounter.getDomain());
					rc.setCountryRank(rankCounter.getCountryRank());
					rc.setGlobalRank(rankCounter.getGlobalRank());
					rc.setValue(rankCounter.getValue());

					rankCountersPositive.add(rc);
				}
				model.addAttribute("positiveMedia", rankCountersPositive);

				// data Negative
				List<RankCounterMedia> listNegative = analyticRepository.getMostPopularNegativeMedia(project, start, end,
						limit);
				List<RankCounterMedia> rankCountersNegative = new ArrayList<>();
				for (RankCounterMedia rankCounter : listNegative) {
					RankCounterMedia rc = new RankCounterMedia();
					rc.setCountryName(rankCounter.getCountryName());
					rc.setDomain(rankCounter.getDomain());
					rc.setCountryRank(rankCounter.getCountryRank());
					rc.setGlobalRank(rankCounter.getGlobalRank());
					rc.setValue(rankCounter.getValue());

					rankCountersNegative.add(rc);
				}
				model.addAttribute("negativeMedia", rankCountersNegative);

				return "analytics/media-reputation/media-reputation";
			} else {
				return "analytics/media-reputation/blank-page";
			}

		} catch (Exception exception) {
			return "analytics/media-reputation/blank-page";
		}
	}

}
