package io.gnews.pro.web.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.gnews.pro.web.config.strings.SuccessMessage;

/**
 * @author masasdani
 * Created Date Oct 27, 2015
 * @param <T>
 */
public class ResponseData<T> {

	private String status;
	private String message;
	@JsonInclude(Include.NON_NULL)
	private T data;

	public ResponseData() {
		this.status = SuccessMessage.SUCCESS;
		this.message = SuccessMessage.DEFAULT_SAVE_SUCCESS;
	}

	public ResponseData(T data) {
		this.status = SuccessMessage.SUCCESS;
		this.message = SuccessMessage.DEFAULT_SAVE_SUCCESS;
		this.data = data;
	}

	public ResponseData(String status, String message) {
		this.status = status;
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
