package io.gnews.pro.web.service;

import java.util.Collections;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.model.mongodb.UserAccount;
import io.gnews.pro.core.repository.mongodb.AccountRepository;
import io.gnews.pro.core.repository.mongodb.UserAccountRepository;

/**
 * @author masasdani
 * Created Date Oct 28, 2015
 */
@Service
public class LoginService implements UserDetailsService, ApplicationListener<AuthenticationSuccessEvent> {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	private UserAccountRepository userAccountRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	
	@PostConstruct
	public void initialize() {
		long size = accountRepository.countByType(Account.TYPE_USER);
		if(size == 0){
				log.info("no user found, create default user !");
				Account account = new Account(
						"developer@gnews.io", 
						true,
						"gnewsdeveloper",
						passwordEncoder.encode("developer"));
				account.setType(Account.TYPE_USER);
				account = accountRepository.save(account);
				
				UserAccount userAccount = new UserAccount();
				userAccount.setFullname("Gnews Developer");
				userAccount.setAccount(account);
				userAccountRepository.save(userAccount);	
			}
	}

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		Account account = accountRepository.findByEmailAndActiveTrue(username);
		if (account == null) {			
			throw new UsernameNotFoundException("user not found");
		}
		return createUser(account);
	}

	/**
	 * @param account
	 */
	public void signin(Account account) {
		SecurityContextHolder.getContext().setAuthentication(
				authenticate(account));
	}

	/**
	 * @param account
	 * @return
	 */
	private Authentication authenticate(Account account) {
		return new UsernamePasswordAuthenticationToken(createUser(account),
				null, Collections.singleton(createAuthority(account)));
	}

	/**
	 * @param account
	 * @return
	 */
	private org.springframework.security.core.userdetails.User createUser(
			Account account) {
		return new org.springframework.security.core.userdetails.User(
				account.getEmail(), account.getPassword(),
				Collections.singleton(createAuthority(account)));
	}

	/**
	 * @param account
	 * @return
	 */
	private GrantedAuthority createAuthority(Account account) {
		return new SimpleGrantedAuthority(account.getType());
	}

	@Override
	public void onApplicationEvent(AuthenticationSuccessEvent event) {
		String userName = ((UserDetails) event.getAuthentication()
				.getPrincipal()).getUsername();
		Account account = accountRepository.findByEmail(userName);
		account.setLastLogin(new Date());
		accountRepository.save(account);
	}

}
