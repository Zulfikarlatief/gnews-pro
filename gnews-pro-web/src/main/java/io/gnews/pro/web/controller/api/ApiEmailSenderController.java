package io.gnews.pro.web.controller.api;

import java.security.Principal;

import javax.servlet.http.HttpSession;

import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.gnews.pro.web.config.strings.ErrorMessage;
import io.gnews.pro.web.config.strings.SuccessMessage;
import io.gnews.pro.web.response.DefaultResponse;
import io.gnews.pro.web.response.ResponseData;
import io.gnews.zimbra.client.DefaultZimbraClient;
import io.gnews.zimbra.client.ZimbraClient;

@RestController
@RequestMapping(value = "/api")
public class ApiEmailSenderController {
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Value("${zimbra.soap.api.url}")
	private String zimbraUrl;
	@Value("${zimbra.soap.api.username}")
	private String zimbraUsername;
	@Value("${zimbra.soap.api.password}")
	private String zimbraPassword;
	
	@RequestMapping(method = RequestMethod.GET, value = "/mail-marketing")
	public <T> Object emailSenderChecker(HttpSession httpSession,
			@RequestParam(required = false) String email,
			Principal principal){
		ZimbraClient zimbraClient = new DefaultZimbraClient(zimbraUrl, HttpClientBuilder.create().build());
		ResponseData<T> responseData = new ResponseData<>();

		if (principal == null) {
			return null;
		}
		
		try {
			if (zimbraClient.auth(zimbraUsername, zimbraPassword)) {
				if (!zimbraClient.userExist(email)) {
					responseData.setStatus(SuccessMessage.SUCCESS);
					responseData.setMessage(SuccessMessage.DEFAULT_SAVE_SUCCESS);
					return responseData;
				} else {
					log.info("testing");
					responseData.setStatus(ErrorMessage.FAILED);
					responseData.setMessage(ErrorMessage.NO_DATA_ERROR);
					return responseData;
					
				}
			}else{
				return DefaultResponse.noDataError();
			}
			
		} catch (Exception e) {
			return DefaultResponse.noDataError();
		}

	}

}
