package io.gnews.pro.web.service.impl;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gnews.pro.core.config.RabbitMQConfig;
import io.gnews.pro.core.model.mongodb.AutomatedReport;
import io.gnews.pro.core.model.mongodb.GnewsLetter;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.model.mongodb.UserAccount;
import io.gnews.pro.core.repository.mongodb.AutomatedReportRepository;
import io.gnews.pro.core.repository.mongodb.GnewsLetterRepository;
import io.gnews.pro.core.repository.mongodb.ProjectRepository;
import io.gnews.pro.core.util.DateUtils;
import io.gnews.pro.web.config.AppConfig;
import io.gnews.pro.web.request.GnewsLetterForm;
import io.gnews.pro.web.request.ProjectForm;
import io.gnews.pro.web.response.ProjectData;
import io.gnews.pro.web.service.ProjectService;
import io.gnews.pro.web.service.UserService;

@Service
public class ProjectServiceImpl implements ProjectService {
	
	private static final String DEFAULT_EXTENSION=".pptx";
	
	private static final String DEFAULT_DAY = "Monday";

	
	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Autowired
	private GnewsLetterRepository gnewsLetterRepository;
	
	@Autowired
	private AutomatedReportRepository automatedReportRepository;
	
	@Override
	public Project createProject(Principal principal, ProjectForm form, HttpSession session) {
		UserAccount account = userService.findUserByEmail(principal.getName());

		Project project = new Project();

		if (form.getId() != null)
			project.setId((form.getId()));

		project.setUser(account);
		project.setName(form.getName());

		Set<String> languange = new HashSet<>();
		for (String s : form.getLanguange()) {
			languange.add(s);
		}
		
		project.setLanguanges(languange);
		project.setKeyword(form.getKeyword());
		project.setFilter(form.getFilter());
		project.setCreatedAt(new Date());
		project.setStatus(Project.PROJECT_INACTIVE);
		project.setActive(false);
		if(account.isRecurrentUser()){
			project.setRecurrent(true);
		}
		project = projectRepository.save(project);
		session.setAttribute(AppConfig.SESSION_DATA_PROJECT_ID, project.getId());
		//save automated report
		AutomatedReport automatedReport = new AutomatedReport();
		automatedReport.setProject(project);
		automatedReport.setReportType(DEFAULT_EXTENSION);
		automatedReport.setDay(DEFAULT_DAY);
		automatedReportRepository.save(automatedReport);
		
		return project;
	}

	@Override
	public boolean deleteProject(String id) {
		Project project = projectRepository.findOne(id);
		List<GnewsLetter> gnewsLetter = gnewsLetterRepository.findByProjectId(project.getId());
		for(GnewsLetter gnewsLetter2 : gnewsLetter){
			gnewsLetter2.setActive(false);
			gnewsLetterRepository.save(gnewsLetter2);
		}
		project.setDeleted(true);
		project.setActive(false);
		projectRepository.save(project);
		return true;
	}

	@Override
	public Project getById(String id) {
		if(id == null) return null;
		return projectRepository.findOne(id);
	}

	
	@Override
	public List<Project> getUserProjects(Principal principal, String type) {
		UserAccount user = userService.findUserByEmail(principal.getName());
		if(type == null){
			return projectRepository.findByUserAndDeletedFalse(user);
		}else{
			return projectRepository.findByUserAndStatusAndDeletedFalse(user, type);
		}
	}

	@Override
	public List<ProjectData> CountTotalDays(Principal principal){
		UserAccount user = userService.findUserByEmail(principal.getName());
		List<Project> getProject = projectRepository.findByUserAndDeletedFalse(user);
		List<ProjectData> projectDatas = new ArrayList<>();
		for(Project project : getProject){
			ProjectData projectData = new ProjectData();
			int days = DateUtils.calculateNumberOfDays(new Date(), project.getExpiredAt());
			projectData.setRange(days);
			projectDatas.add(projectData);
		}
		return projectDatas;
		
	}
	@Override
	public void activate(String id, int period, boolean newProject) {
		Project project = getById(id);
		project.setActive(true);
		project.setStatus(Project.PROJECT_ACTIVE);
		project.setExpired(false);
		project.setDeleted(false);
		if(project.getExpiredAt() == null){
			project.setExpiredAt(new Date());
		}
		project.setExpiredAt(DateUtils.Monthafter(project.getExpiredAt(), period));
		project = projectRepository.save(project);
		if(newProject){
			rabbitTemplate.convertAndSend(RabbitMQConfig.CREATE_PROJECT_QUEUE, project.getId());
		}
	}
	
	@Override
	public GnewsLetter saveGnewsLetter(Principal principal, GnewsLetterForm form) {
		Project project = projectRepository.findOne(form.getId());

		GnewsLetter gnewsLetter = new GnewsLetter();
		gnewsLetter.setEmail(form.getEmail());
		gnewsLetter.setHour(project.getLetterHour());
		gnewsLetter.setFirstTime(true);
		gnewsLetter.setFirstName(form.getFirstName());
		gnewsLetter.setLastName(form.getLastName());
		gnewsLetter.setProject(project);
		gnewsLetter.setActive(true);

		return gnewsLetterRepository.save(gnewsLetter);
	}

	@Override
	public List<GnewsLetter> getGnewsLetterUser(String id) {
		Project project = projectRepository.findOne(id);
		return gnewsLetterRepository.findByProject(project);
	}
	
	@Override
	public void deleteGnewsLetterUser(String id) {
		gnewsLetterRepository.delete(id);
	}
	
	@Override
	public void saveGnewsLetterConfig(Principal principal, GnewsLetterForm form) {
		Project project = projectRepository.findOne(form.getId());
		project.setLetterHour(form.getHour());
		List<GnewsLetter> gnewsLetters = gnewsLetterRepository.findByProject(project);
		List<GnewsLetter> gnewsLetter = new ArrayList<>();
		for(GnewsLetter list : gnewsLetters){
			list.setHour(form.getHour());
			list.setFirstTime(true);
			gnewsLetter.add(list);
		}
		gnewsLetterRepository.save(gnewsLetter);
		projectRepository.save(project);
	}
	
}
