package io.gnews.pro.web.service;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpSession;

import io.gnews.pro.core.model.mongodb.Payment;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.web.request.PaymentMethodForm;
import io.gnews.pro.web.request.PaymentOrderForm;
import io.gnews.pro.web.response.PromoCodeData;

public interface PaymentService {

	public Payment getByid(String id);
	
	public Payment getProjectById(String id);

	public Payment getByInvoice(String invoice);
	
	public Payment createRecurrentProjectInvoice(Project project);

	public Payment savePaymentMethod(Principal principal, HttpSession session, PaymentMethodForm form);
	
	public Payment getPaymentById(String id);
	
	public List<Payment> getPaymentData(Principal principal, String type);

	public PromoCodeData checkPromocode(HttpSession session, String code);

	public Payment savePaymentOrder(Principal principal, HttpSession session, PaymentOrderForm form);

	public void setPaid(Payment payment);

}
