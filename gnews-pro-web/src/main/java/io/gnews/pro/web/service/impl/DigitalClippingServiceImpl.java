package io.gnews.pro.web.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gnews.pro.core.model.dto.ArticleData;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.model.mongodb.WebData;
import io.gnews.pro.core.model.service.GnewsRestService;
import io.gnews.pro.core.repository.mongodb.ProjectRepository;
import io.gnews.pro.web.repository.mongodb.WebDataAnalyticRepository;
import io.gnews.pro.web.request.DigitalClippingForm;
import io.gnews.pro.web.service.DigitalClippingService;
import io.gnews.pro.web.service.TwitterMediaService;

@Service
public class DigitalClippingServiceImpl implements DigitalClippingService{
	
	public static final int MIN_CONTENT_LENGHT = 400;
	public static final int MAX_SNIPPED_LENGHT = 200;

	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private WebDataAnalyticRepository analyticRepository;
	
	@Autowired
	private GnewsRestService gnewsRestService;
	
	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired
	private TwitterMediaService twitterMediaService;
	
	@Override
	public void saveNewUrlDC(Project project, DigitalClippingForm form) {
		log.info("save new url digital clipping");
		String image = gnewsRestService.getKeywordImage(project.getKeyword());
		if (image != null) {
			project.setImage(image);
			projectRepository.save(project);
		}
		if (project != null) {
			twitterMediaService.getWebdata(project.getId(), form.getUrlId(), form.getDate());
		}	
	}

	@Override
	public void deleteArticleDC(Project project, String urlId) {
		analyticRepository.deleteArticleByProjectId(project, urlId);
		log.info("delete article with url "+ urlId + " success");
		
	}
	
	@Override
	public ArticleData editArticleDC(Project project, String urlId) {
		if(project == null){
			return null;
		}
		return analyticRepository.editArticleByProjectId(project, urlId);
	}

	@Override
	public String saveArticleDC(Project project, String urlId, DigitalClippingForm form) {
		if(project != null){
		WebData webData = analyticRepository.getWebData(project, urlId);
		log.info("found the data " + webData);
		webData.setImage(form.getImage());
		webData.setTitle(form.getTitle());
		webData.setDate(form.getDate());
		webData.setContent(form.getContent());
		
		String snipped = webData.getContent();
		log.info(snipped);
		if (snipped.length() <= webData.getTitle().length()) {
			snipped = snipped.substring(0, MAX_SNIPPED_LENGHT);
			webData.setSnippet(snipped);
		} else if (snipped.length() > MAX_SNIPPED_LENGHT) {
			snipped = snipped.substring(0, MAX_SNIPPED_LENGHT);
			webData.setSnippet(snipped);
		}
			
		analyticRepository.saveWebData(project, webData);
		}
		
		return null;
		
	}
}
