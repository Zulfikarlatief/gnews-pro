package io.gnews.pro.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.gnews.pro.web.service.VerificationService;

@Controller
@RequestMapping("/verify")
public class VerifyController {

	@Autowired
	private VerificationService verifyService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/success")
	private String verifySuccess(){
		return "verify-success";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/error")
	private String verifyError(){
		return "verify-errror";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/email/sender/{token}")
	private String verifySender(@PathVariable String token){
		if(verifyService.validateEmailSender(token)){
			return "redirect:/verify/success";
		}else{
			return "redirect:/verify/error";			
		}
	}
	
}
