/*package io.gnews.pro.web.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import io.gnews.pro.core.model.jpa.Project;
import io.gnews.pro.core.repository.jpa.ProjectRepository;
import io.gnews.pro.report.service.ChartGenerator;
import io.gnews.pro.web.repository.mongodb.WebDataAnalyticRepository;
import io.gnews.pro.web.request.DataMarksBean;
import io.gnews.pro.web.response.DataMarksBeanList;
import io.gnews.pro.web.service.ProjectService;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRPptxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.ExporterInputItem;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleExporterInputItem;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

@Controller
@RequestMapping("/automated")
public class ReportController {
	
	public static final String FILE_NAME_PREFIX = "weekly_report";
	public static final String FILE_EXT_PPTX = ".pptx";
	
	@Value("${report.data.doc}")
	private String doc;
	@Value("${report.data.weekly}")
	private String weekly;
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	ResourceLoader resourceLoader;
	
	@Autowired
	ProjectService projectService;
	
	@Autowired
	ProjectRepository projectRepository;
	
	@Autowired
	WebDataAnalyticRepository analyticRepository;
	

	@RequestMapping(value = "{id}/generate-report", method = RequestMethod.GET)
	public String createReport(
			Principal principal, 
			Model model,
			@PathVariable("id") Long id,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date end,
			@RequestParam(required = false) String accessToken, 
			@RequestParam(required = false) Integer offset,
			@RequestParam(required = false) Integer limit, 
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		if (principal == null) {
			return "redirect:/project";
		}
		
		 put some commend here to generate chart 
		chartGenerator.generateWordCloud(id, start, end);
		chartGenerator.generateTopMedia(id, start, end, offset, limit);
		chartGenerator.generateSentimentChart(id, start, end, offset, limit);
		chartGenerator.generatePeakDay(id, start, end, offset, limit);
		chartGenerator.generateMediaReputation(id, start, end, accessToken, offset, limit);

		SimpleDateFormat dateFormat = new SimpleDateFormat("E,MMM dd, yyyy");
		String startD = dateFormat.format(start);
		String endD = dateFormat.format(end);
		Project project = projectService.getById(id);
		Long mediaCount = analyticRepository.getMediaCount(project, start, end);
		
		log.info("hasil media countnya " + mediaCount);
		
		Long newsCount = analyticRepository.getArticleCount(project, start, end);

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("projectName", project.getName());
		parameters.put("keyword", project.getKeyword());
		parameters.put("period", new StringBuilder().append(startD).append(" - ").append(endD));
		parameters.put("countMedia", mediaCount);
		parameters.put("countNews", newsCount);
		List<ExporterInputItem> jpList = new ArrayList<>();
		
		try {
			Resource coverTemp = resourceLoader.getResource("classpath:reports/weekly-gnews-pro-cover.jasper");
			Resource keywordTemp = resourceLoader.getResource("classpath:reports/weekly-keyword-mapping.jasper");
			Resource topMediaTemp = resourceLoader.getResource("classpath:reports/weekly-top-media.jasper");
			Resource sentimentTemp = resourceLoader.getResource("classpath:reports/weekly-sentiment.jasper");
			Resource mediaMappingTemp = resourceLoader.getResource("classpath:reports/weekly-media-reputation.jasper");
			Resource peakDayTemp = resourceLoader.getResource("classpath:reports/weekly-periodic-time.jasper");
			Resource endTemp = resourceLoader.getResource("classpath:reports/weekly-end-cover.jasper");

			Resource logoGnews = resourceLoader.getResource("classpath:reports/logo_gnewspro_big.png");
			BufferedImage logo = ImageIO.read(logoGnews.getInputStream());
			parameters.put("logo", logo);

			File kmSourceImg = new File(weekly + "keywordMapping" + project.getId() + ".png");
			BufferedImage keywordMapping = ImageIO.read(kmSourceImg);
			parameters.put("keywordMapping", keywordMapping);

			File topMediaImg = new File(weekly + "topMedia" + project.getId() + ".png");
			BufferedImage topMediaBuf = ImageIO.read(topMediaImg);
			parameters.put("topMedia", topMediaBuf);

			File sentimentImg = new File(weekly + "sentiment" + project.getId() + ".png");
			BufferedImage sentimentBuf = ImageIO.read(sentimentImg);
			parameters.put("sentiment", sentimentBuf);

			File mediaMappingImg = new File(weekly + "mediareputation" + project.getId() + ".jpg");
			BufferedImage mediaMappingBuf = ImageIO.read(mediaMappingImg);
			parameters.put("mediaReputation", mediaMappingBuf);

			File peakTimeImg = new File(weekly + "peakDay" + project.getId() + ".png");
			BufferedImage peakTimeBuf = ImageIO.read(peakTimeImg);
			parameters.put("peakDay", peakTimeBuf);

			DataMarksBeanList DataBeanList = new DataMarksBeanList();
			ArrayList<DataMarksBean> dataList = DataBeanList.getDataBeanList();
			JRBeanCollectionDataSource one = new JRBeanCollectionDataSource(dataList);
			JRBeanCollectionDataSource two = new JRBeanCollectionDataSource(dataList);
			JRBeanCollectionDataSource three = new JRBeanCollectionDataSource(dataList);
			JRBeanCollectionDataSource four = new JRBeanCollectionDataSource(dataList);
			JRBeanCollectionDataSource five = new JRBeanCollectionDataSource(dataList);
			JRBeanCollectionDataSource six = new JRBeanCollectionDataSource(dataList);
			JRBeanCollectionDataSource seven = new JRBeanCollectionDataSource(dataList);

			// cover
			JasperReport cover = (JasperReport) JRLoader.loadObject(coverTemp.getInputStream());
			JasperPrint coverPrint = JasperFillManager.fillReport(cover, parameters, one);

			JasperReport keyword = (JasperReport) JRLoader.loadObject(keywordTemp.getInputStream());
			JasperPrint keywordPrint = JasperFillManager.fillReport(keyword, parameters, two);

			JasperReport topMedia = (JasperReport) JRLoader.loadObject(topMediaTemp.getInputStream());
			JasperPrint topMediaPrint = JasperFillManager.fillReport(topMedia, parameters, three);

			JasperReport sentiment = (JasperReport) JRLoader.loadObject(sentimentTemp.getInputStream());
			JasperPrint sentimentPrint = JasperFillManager.fillReport(sentiment, parameters, four);

			JasperReport mediaMapping = (JasperReport) JRLoader.loadObject(mediaMappingTemp.getInputStream());
			JasperPrint mediaMappingPrint = JasperFillManager.fillReport(mediaMapping, parameters, five);

			JasperReport peakDay = (JasperReport) JRLoader.loadObject(peakDayTemp.getInputStream());
			JasperPrint peakDayPrint = JasperFillManager.fillReport(peakDay, parameters, six);

			JasperReport endCover = (JasperReport) JRLoader.loadObject(endTemp.getInputStream());
			JasperPrint endCoverPrint = JasperFillManager.fillReport(endCover, parameters, seven);

			jpList.add(new SimpleExporterInputItem(coverPrint));
			jpList.add(new SimpleExporterInputItem(keywordPrint));
			jpList.add(new SimpleExporterInputItem(topMediaPrint));
			jpList.add(new SimpleExporterInputItem(sentimentPrint));
			jpList.add(new SimpleExporterInputItem(mediaMappingPrint));
			jpList.add(new SimpleExporterInputItem(peakDayPrint));
			jpList.add(new SimpleExporterInputItem(endCoverPrint));

			log.info(jpList.size() + "");
			
			JRPptxExporter exporterPPT = new JRPptxExporter();
			exporterPPT.setExporterInput(new SimpleExporterInput(jpList));
			
			OutputStream output = new FileOutputStream(new File(doc + FILE_NAME_PREFIX + project.getId() + FILE_EXT_PPTX));
			exporterPPT.setExporterOutput(new SimpleOutputStreamExporterOutput(output));
			
			exporterPPT.exportReport();

		} catch (JRException e) {
			e.printStackTrace();
		}
		return "redirect:/project/" + id + "/automated-report";
	}
	 
}
*/