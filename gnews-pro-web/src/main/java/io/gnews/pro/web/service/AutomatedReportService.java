package io.gnews.pro.web.service;

import java.security.Principal;

import org.springframework.web.multipart.MultipartFile;

import io.gnews.pro.core.model.dto.AutomaticReportForm;
import io.gnews.pro.core.model.mongodb.AutomatedReport;

public interface AutomatedReportService {
	
	public AutomatedReport saveConfiguration(Principal principal, AutomaticReportForm form, MultipartFile imageUpload);
	
	public AutomatedReport getByProjectId(String id);
	
}
