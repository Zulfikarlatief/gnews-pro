package io.gnews.pro.web.service;

import io.gnews.pro.core.model.mongodb.Account;

public interface AccountService {

	public Account getAccountByToken(String accessToken);

	public Account getAccountByEmail(String email);
	
}
