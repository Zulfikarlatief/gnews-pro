package io.gnews.pro.web.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.base.Throwables;

import io.gnews.pro.web.config.AppConfig;
import io.gnews.pro.web.request.GnewsLetterForm;
import io.gnews.pro.web.service.ProjectService;
import io.gnews.pro.web.service.SiteService;

@Controller
@RequestMapping(value="/project")
public class GnewsLetterController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SiteService siteService;

	@Autowired
	private ProjectService projectService;

	@RequestMapping(value = "/{id}/gnewsletter/start")
	public String gnewsletterstart(
			@PathVariable String id, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Principal principal) {
		try{
			model.addAttribute("project", projectService.getById(id));
			siteService.setSiteInfo(principal, model);
			
			if (projectService.getGnewsLetterUser(id).size() >= 1) {
				return "redirect:/project/" + id + "/gnewsletter/recipients";
			} else {
				return "gnewsletter/gnewsletter-start";
			}
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}
	
	@RequestMapping(value = "/{id}/gnewsletter/recipients")
	public String gnewsletterRecipient(
			@PathVariable String id, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Principal principal) {
		try{
			model.addAttribute("project", projectService.getById(id));
			model.addAttribute("userData", projectService.getGnewsLetterUser(id));
			session.setAttribute(AppConfig.SESSION_DATA_PROJECT_ID, id);
			siteService.setSiteInfo(principal, model);
			
			return "gnewsletter/gnewsletter-recipients";
			
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}
	
	@RequestMapping(value = "/{id}/gnewsletter/report/list")
	public String gnewsletterList(
			@PathVariable String id, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Principal principal) {
		try{
			model.addAttribute("project", projectService.getById(id));
			model.addAttribute("userData", projectService.getGnewsLetterUser(id));
			session.setAttribute(AppConfig.SESSION_DATA_PROJECT_ID, id);
			siteService.setSiteInfo(principal, model);

			return "gnewsletter/gnewsletter-list";
			
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}
	
	@RequestMapping(value = "/{id}/gnewsletter/report/{mailId}")
	public String gnewsletterReport(
			@PathVariable String id,
			@PathVariable Long mailId,
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Principal principal) {
		try{
			model.addAttribute("project", projectService.getById(id));
			model.addAttribute("userData", projectService.getGnewsLetterUser(id));
			session.setAttribute(AppConfig.SESSION_DATA_PROJECT_ID, id);
			siteService.setSiteInfo(principal, model);
			model.addAttribute("mailId", mailId);
			
			return "gnewsletter/gnewsletter-report";
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}  
	
	@RequestMapping(value = "/{id}/gnewsletter/report/delivered/{mailId}")
	public String gnewsletterDelivered(
			@PathVariable String id,
			@PathVariable Long mailId,
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Principal principal) {
		try{
			model.addAttribute("project", projectService.getById(id));
			model.addAttribute("userData", projectService.getGnewsLetterUser(id));
			session.setAttribute(AppConfig.SESSION_DATA_PROJECT_ID, id);
			siteService.setSiteInfo(principal, model);
			
			return "gnewsletter/gnewsletter-delivered-detail";
			
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}  
	
	@RequestMapping(value = "/{id}/gnewsletter/report/opened/{mailId}")
	public String gnewsletterOpened(
			@PathVariable String id,
			@PathVariable Long mailId,
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Principal principal) {
		try{
			model.addAttribute("project", projectService.getById(id));
			model.addAttribute("userData", projectService.getGnewsLetterUser(id));
			session.setAttribute(AppConfig.SESSION_DATA_PROJECT_ID, id);
			siteService.setSiteInfo(principal, model);
			model.addAttribute("mailId", mailId);
			
			return "gnewsletter/gnewsletter-delivered-detail";
			
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}  
	
	@RequestMapping(value = "/{id}/gnewsletter/report/clicked/{mailId}")
	public String gnewsletterClicked(
			@PathVariable String id,
			@PathVariable Long mailId,
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Principal principal) {
		try{
			model.addAttribute("project", projectService.getById(id));
			model.addAttribute("userData", projectService.getGnewsLetterUser(id));
			session.setAttribute(AppConfig.SESSION_DATA_PROJECT_ID, id);
			siteService.setSiteInfo(principal, model);
			model.addAttribute("mailId", mailId);
			
			return "gnewsletter/gnewsletter-clicked-all";
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}  
	
	@RequestMapping(value = "/{id}/gnewsletter/report/clicked/{mailId}/detail/{urlId}")
	public String gnewsletterClickedDetail(
			@PathVariable String id,
			@PathVariable Long mailId,
			@PathVariable String urlId,
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Principal principal) {
		try{
			model.addAttribute("project", projectService.getById(id));
			model.addAttribute("userData", projectService.getGnewsLetterUser(id));
			session.setAttribute(AppConfig.SESSION_DATA_PROJECT_ID, id);
			siteService.setSiteInfo(principal, model);
			model.addAttribute("mailId", mailId);
			model.addAttribute("urlId", urlId);
			
			return "gnewsletter/gnewsletter-clicked-detail";
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}  
	
	@RequestMapping(value = "/{id}/gnewsletter/report/unsubscribe/{mailId}")
	public String gnewsletterUnsubscribe(
			@PathVariable String id,
			@PathVariable Long mailId,
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Principal principal) {
		try{
			model.addAttribute("project", projectService.getById(id));
			model.addAttribute("userData", projectService.getGnewsLetterUser(id));
			session.setAttribute(AppConfig.SESSION_DATA_PROJECT_ID, id);
			siteService.setSiteInfo(principal, model);
			model.addAttribute("mailId", mailId);
			
			return "gnewsletter/gnewsletter-unsubscribe-detail";
			
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}  
	
	@RequestMapping(method = RequestMethod.POST, value="/save/gnewsletter")
	public String saveGnewsletter(
			Principal principal,
			Model model,
			@ModelAttribute GnewsLetterForm form){
		
		if(principal == null){
			return "redirect:/";
		}		
		projectService.saveGnewsLetter(principal, form);
		
		return "redirect:/project/"+form.getId()+"/gnewsletter/recipients";
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/save/gnewsletterConfig")
	public String saveGnewsletterConfig(
			Principal principal,
			Model model,
			@ModelAttribute GnewsLetterForm form){
		
		if(principal == null){
			return "redirect:/";
		}		
		projectService.saveGnewsLetterConfig(principal, form);
		
		return "redirect:/project/"+form.getId()+"/gnewsletter/recipients";
	}
	
	@RequestMapping(value = "/delete/gnewsletter")
	public String delete(
			Principal principal, 
			Model model, 
			HttpSession session,
			@RequestParam String id,
			@ModelAttribute GnewsLetterForm form) {		
		if (principal == null) {
			return "redirect:/";
		}
		try {
			projectService.deleteGnewsLetterUser(id);
			Long idProject = Long.parseLong(session.getAttribute(AppConfig.SESSION_DATA_PROJECT_ID).toString());
			session.removeAttribute(AppConfig.SESSION_DATA_PROJECT_ID);

			return "redirect:/project/" + idProject + "/gnewsletter/recipients";
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return null;

	}
	
}
