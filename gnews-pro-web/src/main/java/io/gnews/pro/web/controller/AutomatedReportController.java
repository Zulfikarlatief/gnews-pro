package io.gnews.pro.web.controller;

import java.security.Principal;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.base.Throwables;

import io.gnews.pro.core.config.RabbitMQConfig;
import io.gnews.pro.core.model.dto.AutomaticReportForm;
import io.gnews.pro.web.service.AutomatedReportService;
import io.gnews.pro.web.service.ProjectService;
import io.gnews.pro.web.service.SiteService;

@Controller
@RequestMapping(value = "/project")
public class AutomatedReportController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	private static final int LIMIT = 10;
	private static final int OFFSET = 0;
	
	@Autowired
	private SiteService siteService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private AutomatedReportService automatedReportService;

	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	
	@RequestMapping(value = "/{id}/automated-report/start")
	public String automatedstart(
			@PathVariable String id, 
			Model model,
			HttpSession session,
			HttpServletRequest request,
			HttpServletResponse response,
			Principal principal) {
		try {
						model.addAttribute("ar", automatedReportService.getByProjectId(id));
			model.addAttribute("project", projectService.getById(id));
			model.addAttribute("idProject", id);
			siteService.setSiteInfo(principal, model);

			if (projectService.getById(id).getHour() < 0) {
				return "automated-report/automated-report";
			} else {
				return "redirect:/project/" + id + "/automated-report";
			}

		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}

	@RequestMapping(value = "/{id}/automated-report")
	public String automated(
			@PathVariable String id, 
			Model model,
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response, 
			Principal principal) {
		try {
			model.addAttribute("ar", automatedReportService.getByProjectId(id));
			model.addAttribute("project", projectService.getById(id));
			model.addAttribute("idProject", id);
			siteService.setSiteInfo(principal, model);

			return "automated-report/automated-report";
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}

	@RequestMapping(value = "/{id}/automated-report/custom")
	public String automatedcustom(
			@PathVariable String id, 
			Model model,
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response, 
			Principal principal) {
		try {
			model.addAttribute("ar", automatedReportService.getByProjectId(id));
			model.addAttribute("project", projectService.getById(id));
			model.addAttribute("idProject", id);
			siteService.setSiteInfo(principal, model);

			return "automated-report/automated-report-custom";
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/save/ar/config")
	public String saveConfig(
			Principal principal,
			Model model, 
			@ModelAttribute AutomaticReportForm form,
			@RequestParam("image") MultipartFile image
			){
		if (principal == null) {
			return "redirect:/project";
		}
		automatedReportService.saveConfiguration(principal, form, image);
		return "redirect:/project/" + form.getId() + "/automated-report";
	}
	

	@RequestMapping(value = "/generate/report",method = RequestMethod.POST)
	public String createReport(
			Principal principal,
			Model model,
			@ModelAttribute AutomaticReportForm form,
			HttpServletRequest request, HttpServletResponse response)
			{

		if (principal == null) {
			return "redirect:/project";
		}
		model.addAttribute("ar", automatedReportService.getByProjectId(form.getId()));
		model.addAttribute("project", projectService.getById(form.getId()));
		siteService.setSiteInfo(principal, model);		
		
		try {	
			//set queue to rabbitMQ
			AutomaticReportForm arForm = new AutomaticReportForm();
			arForm.setStart(form.getStart());
			arForm.setEnd(form.getEnd());
			arForm.setId(form.getId());
			arForm.setLimit(LIMIT);
			arForm.setOffset(OFFSET);
			
			rabbitTemplate.convertAndSend(RabbitMQConfig.GENERATE_WEEKLY_AR, arForm);
			
			log.info("success");
			model.addAttribute("ar", automatedReportService.getByProjectId(form.getId()));
			model.addAttribute("project", projectService.getById(form.getId()));
			model.addAttribute("idProject", form.getId());
			siteService.setSiteInfo(principal, model);
			return "automated-report/custom-report-success";
		} catch (Exception e) {
			log.info("failed : ", e);
			return null;
		}
	}
}