package io.gnews.pro.web.request;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class DigitalClippingForm {

	public String idProject;
	public String urlId;
	private String title;
	private String image;
	private String content;
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm")
	private Date date;
	
	public DigitalClippingForm() {
		// TODO Auto-generated constructor stub
	}

	public String getIdProject() {
		return idProject;
	}

	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}

	public String getUrlId() {
		return urlId;
	}

	public void setUrlId(String urlId) {
		this.urlId = urlId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}	
}
