package io.gnews.pro.web.service;

import io.gnews.pro.core.model.dto.ArticleData;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.web.request.DigitalClippingForm;

public interface DigitalClippingService {
	
	public void deleteArticleDC(Project project, String urlId);
	
	public ArticleData editArticleDC(Project project, String urlId);
	
	public String saveArticleDC(Project project, String urlId, DigitalClippingForm form);

	public void saveNewUrlDC(Project project, DigitalClippingForm form);

}
