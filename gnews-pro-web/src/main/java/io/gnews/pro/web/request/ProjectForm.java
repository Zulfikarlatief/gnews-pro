package io.gnews.pro.web.request;

public class ProjectForm {
	
	private String id;
	private String name;
	private String type;
	private String filter;
	private String keyword;
	private String[] languange;
	
	public ProjectForm() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String[] getLanguange() {
		return languange;
	}

	public void setLanguange(String[] languange) {
		this.languange = languange;
	}
	
}
