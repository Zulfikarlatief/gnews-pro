package io.gnews.pro.web.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import io.gnews.pro.web.util.ImageUtil;

@Service
public class UploadService {

	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Value("${upload.data.dir}")
	private String dataDir;
	
	public static final String UPLOAD_DIR= "uploads/";
	public static final String IMAGE_DIR= "images/";
	public static final String THUMBNAILS_DIR= "thumbnails/";
	public static final int DEFAULT_THUMBNAIL_WIDTH = 200;
	public static final int DEFAULT_THUMBNAIL_HEIGHT = 200;
	
	/**
	 * @param folderName
	 * @param upload
	 * @param createThumbnail
	 * @return
	 * @throws IOException
	 */
	public String uploadImage(String folderName, MultipartFile upload, boolean createThumbnail) throws IOException{
		return uploadImage(folderName, upload.getOriginalFilename(), upload, createThumbnail);
	}
		
	/**
	 * @param folderName
	 * @param name
	 * @param upload
	 * @param createThumbnail
	 * @return
	 * @throws IOException
	 */
	public String uploadImage(String folderName, String name, MultipartFile upload, boolean createThumbnail) throws IOException{
		if(createThumbnail){
			return uploadImage(folderName, name, upload, 0, 0);
		}else{
			return uploadImage(folderName, name, upload, DEFAULT_THUMBNAIL_WIDTH, DEFAULT_THUMBNAIL_HEIGHT);
		}
	}
	
	/**
	 * @param folderName
	 * @param name
	 * @param upload
	 * @param thumbnailWidth
	 * @param thumbnailHeight
	 * @return
	 * @throws IOException
	 */
	public String uploadImage(String folderName, String name, MultipartFile upload, int thumbnailWidth, int thumbnailHeight) throws IOException{
		if(upload.getContentType().contains("image")){
			String dirString = dataDir + UPLOAD_DIR + IMAGE_DIR + folderName;
			File dir = new File(dirString);
			if(!dir.exists()){
				dir.mkdirs();
			}
			String filename = dir.getAbsolutePath()+"/"+name;
			BufferedImage src = ImageIO.read(new ByteArrayInputStream(upload.getBytes()));
			String extension = FilenameUtils.getExtension(name);
			ImageIO.write(src, extension, new File(filename));
			log.info("write image on : " + filename);
			if(thumbnailWidth != 0 && thumbnailHeight != 0){
	        	File thumbnailDir = new File(dirString+THUMBNAILS_DIR);
				if(!thumbnailDir.exists()){
					thumbnailDir.mkdirs();
				}
				filename = thumbnailDir.getAbsolutePath()+"/"+upload.getOriginalFilename();
				BufferedImage cropImage = ImageUtil.smartCrop(src, thumbnailWidth, thumbnailHeight);
				ImageIO.write(cropImage, extension, new File(filename));
				log.info("write image thumbnail on : " + filename);
			}
	        return upload.getOriginalFilename();
		}else{
			return null;
		}
	}
	
	/**
	 * @param folderName
	 * @param name
	 * @param upload
	 * @param thumbnailWidth
	 * @param thumbnailHeight
	 * @return
	 * @throws IOException
	 */
	public String uploadImageAndResize(String folderName, String name, MultipartFile upload, int width, int height) throws IOException{
		if(upload.getContentType().contains("image")){
			String dirString = dataDir + UPLOAD_DIR + IMAGE_DIR + folderName;
			File dir = new File(dirString);
			if(!dir.exists()){
				dir.mkdirs();
			}
			String filename = dir.getAbsolutePath()+"/"+name;
			BufferedImage src = ImageIO.read(new ByteArrayInputStream(upload.getBytes()));
			BufferedImage cropImage = ImageUtil.smartCrop(src, width, height);
			String extension = FilenameUtils.getExtension(name);
			ImageIO.write(cropImage, extension, new File(filename));
			log.info("write image on : " + filename);
			return upload.getOriginalFilename();
		}else{
			return null;
		}
	}
	
	public String uploadCompanyLogo(String folderName, String id, String name, MultipartFile upload, int width, int height) throws IOException{
		if(upload.getContentType().contains("image")){
			String dirString = folderName;
			File dir = new File(dirString);
			if(!dir.exists()){
				dir.mkdirs();
			}
			String filename = dir.getAbsolutePath()+"/"+id+name;
			log.info(filename);
			BufferedImage src = ImageIO.read(new ByteArrayInputStream(upload.getBytes()));
			BufferedImage cropImage = ImageUtil.smartCrop(src, width, height);
			String extension = FilenameUtils.getExtension(name);
			ImageIO.write(cropImage, extension, new File(filename));
			log.info("write image on : " + filename);
			return upload.getOriginalFilename();
		}else{
			return null;
		}
	}
}
