package io.gnews.pro.web;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.gnews.pro.core.model.mongodb.EmailRecipient;

public class ProgramTest {
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Test
	public void importXLSContact(){
		try {
			Workbook wb = WorkbookFactory.create(new File("/Users/madqori/Downloads/con.xlsx"));
			Sheet mySheet = wb.getSheetAt(0);
			Iterator<Row> rowIterator = mySheet.rowIterator();
			List<EmailRecipient> emailRecipients = new ArrayList<>();

			while (rowIterator.hasNext()) {
				EmailRecipient emailReceipient = new EmailRecipient();

				Row row = rowIterator.next();
				String email = row.getCell(0).getRichStringCellValue().toString();
				String name = row.getCell(1).getRichStringCellValue().toString();
				String firstName = row.getCell(2).getRichStringCellValue().toString();
				String lastName = row.getCell(3).getRichStringCellValue().toString();

				emailReceipient.setEmail(email);
				emailReceipient.setName(name);
				emailReceipient.setFirstName(firstName);
				emailReceipient.setLastName(lastName);

				if (name == "") {
					emailReceipient.setName(firstName + " " + lastName);
				}
				if (firstName == "") {
					emailReceipient.setFirstName(name.substring(0, name.indexOf(' ')));
				}
				if (lastName == "") {
					emailReceipient.setLastName(name.substring(name.indexOf(' ')).trim());
				}

				emailRecipients.add(emailReceipient);
			}
			for (EmailRecipient e : emailRecipients) {
				log.info(e.getEmail());
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		}

		
	}
	

}
