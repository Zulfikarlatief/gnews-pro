$(function () {
	    $('#periodic-chart').highcharts({
	        chart: {
	            type: 'area',
	            spacingBottom: 30
	        },
	        title: {
	            text: 'Periodic Time'
	        },
	        subtitle: {
	            /*text: '* Jane\'s banana consumption is unknown',*/
	            floating: true,
	            align: 'right',
	            verticalAlign: 'bottom',
	            y: 15
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'left',
	            verticalAlign: 'top',
	            x: 150,
	            y: 100,
	            floating: true,
	            borderWidth: 1,
	            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
	        },
	        xAxis: {
	            categories: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Monday']
	        },
	        yAxis: {
	            title: {
	                text: ''
	            },
	            labels: {
	                formatter: function () {
	                    return this.value;
	                }
	            }
	        },
	        tooltip: {
	            formatter: function () {
	                return '<b>' + this.series.name + '</b><br/>' +
	                    this.x + ': ' + this.y;
	            }
	        },
	        plotOptions: {
	            area: {
	                fillOpacity: 0.5
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        series: [{
	            name: 'John',
	            data: [0, 1, 4, 4, 5, 2, 3]
	        }]
	    });
	});