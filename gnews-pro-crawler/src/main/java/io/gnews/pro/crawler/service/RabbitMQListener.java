package io.gnews.pro.crawler.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import io.gnews.pro.core.config.AppConfig;
import io.gnews.pro.core.config.RabbitMQConfig;
import io.gnews.pro.core.model.dto.AutomaticReportForm;
<<<<<<< HEAD
import io.gnews.pro.core.model.mongodb.Project;
=======
import io.gnews.pro.core.model.jpa.Project;
import io.gnews.pro.core.repository.jpa.ProjectRepository;
>>>>>>> fec2f61672eb3ca6371249478a43c9822f9e86d2

@Component
public class RabbitMQListener {
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private CrawlerService crawlerService;
	
	@Autowired
	private ProjectRepository projectRepository;
	
	@RabbitListener(queues  = RabbitMQConfig.CREATE_PROJECT_QUEUE)
	public void createProject(@Payload String projectId){
		Project project = crawlerService.getById(projectId);
		crawlerService.updateData(project);
	}
	
	@RabbitListener(queues = RabbitMQConfig.EMERGENCY_UPDATE_ALL_KM)
	public void updateKM(@Payload Long a){
		log.info("Update keyword mapping for all project");
		crawlerService.updateAllProjectOnlyKM();
	}
	
	@RabbitListener(queues = RabbitMQConfig.EMERGENCY_UPDATE_KM)
	public void updateSpesificKM(@Payload AutomaticReportForm form){
		log.info("Update project with id number " + form.getId());
		Project project = crawlerService.getById(form.getId());
		crawlerService.updateSpesificKM(project, form.getStart(), form.getEnd());
	}
	
	@RabbitListener(queues = RabbitMQConfig.EMERGENCY_UPDATE_ALL_PROJECT)
	public void updateData(@Payload Long a){
		log.info("starting scheduled update for all projects");
		crawlerService.updateAllProjects(AppConfig.TYPE_UPDATE[0]);
	}
	
	@RabbitListener(queues = RabbitMQConfig.EMERGENCY_PROJECT)
	public void updateSepesificProject(@Payload AutomaticReportForm form){
		log.info("start update project num " + form.getId() + "start " + form.getStart() + "end" + form.getEnd());
		Project project = projectRepository.findOne(form.getId());
		crawlerService.updateData(project, form.getStart(), form.getEnd(),AppConfig.TYPE_UPDATE[0] );
		
	}
}


