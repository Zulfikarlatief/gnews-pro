package io.gnews.pro.crawler.model.rest;

import java.util.List;

/**
 * @author masasdani
 * Created Date Oct 4, 2015
 * @param <T>
 */
public class ResponseDataTokenize {

	private String status;
	private String message;
	private List<String> data;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getData() {
		return data;
	}

	public void setData(List<String> data) {
		this.data = data;
	}
	
}
