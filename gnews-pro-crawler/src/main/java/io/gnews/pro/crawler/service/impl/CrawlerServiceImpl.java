package io.gnews.pro.crawler.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gnews.pro.core.config.AppConfig;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.model.mongodb.WebData;
import io.gnews.pro.core.model.service.GnewsRestService;
import io.gnews.pro.core.repository.mongodb.ProjectRepository;
import io.gnews.pro.core.util.DateUtils;
import io.gnews.pro.core.util.DomainUtil;
import io.gnews.pro.crawler.model.rest.Document;
import io.gnews.pro.crawler.model.rest.Sentence;
import io.gnews.pro.crawler.model.rest.Sentiment;
import io.gnews.pro.crawler.repository.WebDataCrawlerRepository;
import io.gnews.pro.crawler.service.CrawlerService;
import io.gnews.pro.crawler.service.GenesisRestService;

@Service
public class CrawlerServiceImpl implements CrawlerService {

	private static int WAITING_TIME = 300000;
		
	@Autowired
	private WebDataCrawlerRepository analyticRepository;
	
	@Autowired
	private GnewsRestService gnewsRestService;
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired
	private GenesisRestService genesisRestService;
	
	@Override
	public Project getById(String id) {
		if(id == null) return null;
		return projectRepository.findOne(id);
	}
	
	@Override
	public void updateData(Project project, Date start, Date end, String typeUpdate) {
		log.info("start update data");
		project.setCrawlState(AppConfig.CRAWLING_PENDING);
		String image = gnewsRestService.getKeywordImage(project.getKeyword());
		if(image != null){
			project.setImage(image);
			projectRepository.save(project);
		}
		if(typeUpdate.equals(AppConfig.TYPE_UPDATE[1]))
			project.setLastUpdate(new Date());
			projectRepository.save(project);
		int offset = 0;
		int limit = 10;
		boolean running = true;
		int saved = 0;
		while (running) {
			List<WebData> list = gnewsRestService.getData(
					project.getKeyword(), 
					String.join(",", project.getLanguanges()), 
					project.getFilter(), 
					limit, 
					offset);	
			if (list.size() > 0) {
				offset += limit;
				log.info("found news : "+list.size() +" in "+offset);
				for(WebData news : list){
					log.info("found news : "+news.getDate() +" " + news.getUrl());
					if (news.getDate().before(end)) {
						if (news.getDate().after(start)) {
							saved++;
							WebData webData = doAnalytic(news);
							analyticRepository.saveWebData(project, webData);
							log.info("saved " + news.getUrl());
						}else{
							running = false;
						}
					}
				}
			}else{
				running = false;
			}
			if (saved > 0 && saved < limit) {
				try {
					updateProjectData(project, start, end);
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
		}
		try {
			updateProjectData(project, start, end);
			updateKeywordMappingData(project, start, end);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		project.setCrawlState(AppConfig.CRAWLING_FINISH);
		projectRepository.save(project);
		log.info("saved : " + saved);
	}
	
	@Override
	public void updateAllProjects(String typeUpdate) {
		final Date end = new Date();
		int iterate = 0;
		List<Project> projects = projectRepository.findByActiveTrue();
		for (Project project : projects) {
			if (iterate < 10) {
				iterate++;
				new Thread(new Runnable() {
					@Override
					public void run() {
						Date start = project.getLastUpdate();
						if (start == null) {
							start = DateUtils.before(end, 7);
						}
						updateData(project, start, end, typeUpdate);
					}

				}).start();
			} else {
				try {
					Thread.sleep(WAITING_TIME);
					iterate = 0;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public void unUpdatedKeywordMapping(){
		final Date end = new Date();
		final Date start = DateUtils.before(end, 1);
		List<Project> projects = projectRepository.findByActiveTrue();
		for (Project project : projects){
			analyticRepository.UnUpdatedKeywordMapping(project, start, end);
		}
	}
	
	@Override
	public void updateAllProjectOnlyKM() {
		final Date end = new Date();
		List<Project> projects = projectRepository.findByActiveTrue();
		for (Project project : projects) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					Date start = DateUtils.before(end, 7);
					updateKeywordMappingData(project, start, end);
				}

			}).start();
		}
	}
	
	//using for emergency update Keyword Mapping
	@Override
	public void updateSpesificKM(Project project, Date start, Date end){
		updateKeywordMappingData(project, start, end);
	}
	
	@Override
	public void updateProjectData(Project project, Date start, Date end) {
		Date startNew = DateUtils.before(end, 7);
		analyticRepository.updateMostPopularMediaRanking(project, startNew, end);
	}
		
	@Override
	public void updateKeywordMappingData(Project project, Date start, Date end){
		log.info("update keyword mappings");
		int iterate = DateUtils.calculateNumberOfDays(start, end);
		for(int i = 1; i < iterate ; i++){
				Date newDate = DateUtils.before(end, i);
				Date newEnd = DateUtils.after(newDate, 1);
				log.info("interate num " + i + "with New Date" + newDate);
				analyticRepository.UnUpdatedKeywordMapping(project, newDate, newEnd);
		}	
	}
	
	@Override
	public void updateData(Project project) {
		Date end = new Date();
		Date start = new DateTime(end).minusDays(7).dayOfMonth().roundFloorCopy().toDate();
		gnewsRestService.createAnalyticKeyword(project.getKeyword());
		updateData(project, start, end, AppConfig.TYPE_UPDATE[1]);
	}
	
	private WebData doAnalytic(WebData news){
		WebData webData = new WebData();
		String 	domain = DomainUtil.getTopPrivateDomain(news.getHost());
		webData.setDomain(domain);
		webData.setSnippet(news.getSnippet());
		
		try{
			Document document = genesisRestService.getSentimentAnalysis(news.getContent()).getData();
			webData.setSentiment(getSentiment(document));
			webData.setNegatives(getNegative(document));
			webData.setPositives(getPositive(document));
		}catch(Exception e){
			log.error(e.getMessage());
		}
		
		try{
			List<String> words = genesisRestService.getTokenizeText(news.getContent()).getData();
			Set<String> terms = new HashSet<>(words);
			webData.setTerms(new ArrayList<>(terms));
		}catch(Exception e){
			log.error(e.getMessage());
		}
		
		DateTime dateTime = new DateTime(news.getDate());
		webData.setPeakDay(dateTime.dayOfMonth().roundFloorCopy().toDate().getTime());
		webData.setPeakHour(dateTime.hourOfDay().roundFloorCopy().toDate().getTime());
		webData.setDate(news.getDate());
		webData.setContent(news.getContent());
		webData.setHost(news.getHost());
		webData.setImage(news.getImage());
		webData.setLang(news.getLang());
		webData.setName(news.getName());
		webData.setTitle(news.getTitle());
		webData.setUrl(news.getUrl());
		return webData;
	}

	/**
	 * @param document
	 * @return
	 */
	private int getSentiment(Document document){
		if(document.getSentiment() == Sentiment.POSITIVE)
			return 1;
		if(document.getSentiment() == Sentiment.NEGATIVE)
			return -1;
		return 0;	
	}
	
	/**
	 * @param document
	 * @return
	 */
	private List<String> getPositive(Document document){
		List<String> list = new ArrayList<String>();
		for(Sentence sentence : document.getSentences()){
			list.addAll(sentence.getPositives());
		}
		return list;
	}
	
	/**
	 * @param document
	 * @return
	 */
	private List<String> getNegative(Document document){
		List<String> list = new ArrayList<String>();
		for(Sentence sentence : document.getSentences()){
			list.addAll(sentence.getNegatives());
		}
		return list;
	}
	
	/**
	 * @param languages
	 * @return
	 */
	public String[] parseLanguage(String languages) {
		return languages.split(",");
	}
}
