package io.gnews.pro.report.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gnews.pro.core.model.mongodb.AutomatedReport;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.repository.mongodb.AutomatedReportRepository;
import io.gnews.pro.core.repository.mongodb.ProjectRepository;
import io.gnews.pro.report.service.ReportService;

@Service
public class ReportServiceImpl implements ReportService{

	private static final int WEEK = 4;
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private AutomatedReportRepository automatedReportRepository;
	
	@Autowired
	private ProjectRepository projectRepository;

	@Override
	public List<AutomatedReport> getAllActiveProject() {
		SimpleDateFormat simpleDate = new SimpleDateFormat("EEE");
		String day = simpleDate.format(new Date());
		DateTime dateTime = new DateTime();
		int hour = dateTime.getHourOfDay();
		return automatedReportRepository.findByActiveTrueAndDayAndHour(day, hour);
	}
	
	@Override
	public List<AutomatedReport> getAllActiveMonthProject() {
		return automatedReportRepository.findByActiveTrueAndMonthlyCount(WEEK);
	}

	@Override
	public Project getById(String id) {
		return projectRepository.findOne(id);
	}
	
	@Override
	public List<Project> projectExpiredChecker() {
		Date date = new Date();
		List<Project> projectActive = projectRepository.findByActiveTrue();
		List<Project> projects = new ArrayList<>();
		for(Project project : projectActive){
			if(project.getExpiredAt().before(date)){
				project.setActive(false);
				project.setExpired(true);
				projects.add(project);
			}
		}
		log.info("Finish Checking Project Expired");
		return projectRepository.save(projects);
	}
	

}
