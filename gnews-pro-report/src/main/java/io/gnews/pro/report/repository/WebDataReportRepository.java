package io.gnews.pro.report.repository;

import java.util.Date;
import java.util.List;

import io.gnews.pro.core.model.dto.PeakCounter;
import io.gnews.pro.core.model.dto.RankCounterMedia;
import io.gnews.pro.core.model.dto.SentimentPeakCounter;
import io.gnews.pro.core.model.dto.TermCounter;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.model.mongodb.WebData;

/**
 * @author masasdani 
 * Created Date Nov 3, 2015
 */
public interface WebDataReportRepository {

	/**
	 * @param project
	 * @param url
	 */
	public WebData getWebData(Project project, String url);
	
	/**
	 * @param project
	 * @param webData
	 */
	public void saveWebData(Project project, WebData webData);
	
	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Long getMediaCount(Project project, Date startDate,
			Date endDate);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Long getMediaMonthlyCount(Project project, Date startDate,
			Date endDate);
	
	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public Long getArticleCount(Project project, Date startDate,
			Date endDate);
	
	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 */
	
	public Long getArticleMonthlyCount(Project project,Date starDate,Date endDate);
	
	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<TermCounter> getMostMedia(Project project, Date startDate,
			Date endDate, int limit, int offset);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<RankCounterMedia> getMostPopularMedia(Project project, Date startDate,
			Date endDate);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<RankCounterMedia> getMostPopularPositiveMedia(Project project, Date startDate,
			Date endDate, int limit);
	
	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<RankCounterMedia> getMostPopularNegativeMedia(Project project, Date startDate,
			Date endDate, int limit);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PeakCounter> getPeakDay(Project project, Date startDate,
			Date endDate);
	
	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */

	public List<SentimentPeakCounter> getSentimentPositiveEachDay(Project project, Date startDate, Date endDate);
	
	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<SentimentPeakCounter> getSentimentNegativeEachDay(Project project, Date startDate, Date endDate);
	
	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<SentimentPeakCounter> getSentimentNeutralEachDay(Project project, Date startDate, Date endDate);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<TermCounter> getKeywordMappingNew(Project project, Date startDate,
			Date endDate);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<TermCounter> getMostMediaNegative(Project project, Date startDate, Date endDate, int limit, int offset);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<TermCounter> getMostMediaPositive(Project project, Date startDate, Date endDate, int limit, int offset);

	/**
	 * @param project
	 * @param startDate
	 * @param endDate
	 * @param limit
	 * @param offset
	 * @return
	 */
	public List<TermCounter> getMostMediaNeutral(Project project, Date startDate, Date endDate, int limit, int offset);

}
