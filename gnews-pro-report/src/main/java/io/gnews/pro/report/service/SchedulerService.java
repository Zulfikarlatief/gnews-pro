package io.gnews.pro.report.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import io.gnews.pro.core.config.AppConfig;
import io.gnews.pro.core.model.mongodb.AutomatedReport;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.repository.mongodb.AutomatedReportRepository;
import io.gnews.pro.core.repository.mongodb.ProjectRepository;
import io.gnews.pro.core.util.DateUtils;

@Service
public class SchedulerService {

	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	private static final String DEFAULT_EXTENSION=".pptx";
	
	@Autowired
	private ReportService projectService;
	
	@Autowired
	private AutomatedReportService automatedReportService;
	
	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired
	private AutomatedReportRepository arRepository;
	
	@Scheduled(cron = "0 0 0 * * ?")
	public void projectExpiredChecker(){
		log.info("Starting scheduled project Expired Checker");
		projectService.projectExpiredChecker();
	}
	
	@Scheduled(cron = "0 0 * * * ?")
	public void weeklyReport() throws IOException{
		log.info("Starting scheduled update for Weekly Aumatic Reports");
		List<AutomatedReport> projects = projectService.getAllActiveProject();
		for (final AutomatedReport project : projects) {
				new Thread(new Runnable() {
					public void run() {
						try {
							log.info("start thread " + project.getProject().getId());
							automatedReportService.generatePDFJasperChart(
									project.getProject().getId(), 
									DateUtils.before(new Date(), 
									7),
									new Date(), AppConfig.STATIC_ACCESSTOKEN, 0, 10);
						} catch (IOException e) {
							e.printStackTrace();
							log.error(e.getMessage());
						}
					}
				}).start();
			}
		
		List<AutomatedReport> monthlyProject = projectService.getAllActiveMonthProject();
		if (monthlyProject != null) {
			log.info("Starting scheduled update for Monthly Automatic Reports");
			for (final AutomatedReport project : monthlyProject) {
				new Thread(new Runnable() {
					public void run() {
						try {
							automatedReportService.generatePDFJasperChartMonthly(project.getProject().getId(),
									new Date(), AppConfig.STATIC_ACCESSTOKEN, 0, 10);
						} catch (IOException e) {
							e.printStackTrace();
							log.error(e.getMessage());
						}
					}
				}).start();
			}
		}
	}
	
	public void updateAutomaticReportData(){
		log.info("Start update data Automated Report Configuration");
		List<Project> list = projectRepository.findAll();
		for(Project project : list ){
			AutomatedReport automatedReport = new AutomatedReport();
			automatedReport.setProject(project);
			automatedReport.setReportType(DEFAULT_EXTENSION);
			arRepository.save(automatedReport);
		}
	}
}
	

