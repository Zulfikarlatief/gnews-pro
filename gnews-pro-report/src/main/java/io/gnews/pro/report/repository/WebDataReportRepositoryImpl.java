package io.gnews.pro.report.repository;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.previousOperation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.skip;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import io.gnews.pro.core.model.dto.PeakCounter;
import io.gnews.pro.core.model.dto.RankCounter;
import io.gnews.pro.core.model.dto.RankCounterMedia;
import io.gnews.pro.core.model.dto.SentimentPeakCounter;
import io.gnews.pro.core.model.dto.TermCounter;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.model.mongodb.WebData;
import io.gnews.pro.core.model.service.AlexaRankService;
import io.gnews.pro.core.model.service.AlexaRankService.AlexaResult;

/**
 * @author masasdani
 * Created Date Nov 4, 2015
 */
@Repository
public class WebDataReportRepositoryImpl implements WebDataReportRepository {
	
	public static final String BASE_COLLECTION = "web_data";  
	
	public static final String MEDIA_RANK_COLLECTION = "media_ranking"; 
	
	public static final String MEDIA_RANK_COLLECTION_POSITIVE = "media_ranking_positive"; 
	
	public static final String MEDIA_RANK_COLLECTION_NEGATIVE = "media_ranking_negative"; 
	
	public static final String MEDIA_RANK_COLLECTION_GLOBAL = "media_ranking_global";
	
	public static final String KEYWORD_MAPPING_COLLECTION = "keyword_mapping"; 
	
	public static final String KEYWORD_MAPPING_POSITIVE_COLLECTION = "keyword_mapping_positive";
	
	public static final String KEYWORD_MAPPING_NEGATIVE_COLLECTION = "keyword_mapping_negative";
	
	public static final String PEAKDAY_SENTIMENT_COLLECTION = "peak_day_sentiment";
	
	public static final String COLLECTION_SPARATOT = "_"; 
	
	private static final int MAX_RELATED_TERMS = 100;
	
	private static final int MAX_MEDIA_RANK_RESULT = 20;

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private AlexaRankService alexaRankService;
	
	/**
	 * @param project
	 * @return
	 */
	private String getCollection(Project project){
		return BASE_COLLECTION + COLLECTION_SPARATOT + project.getId();
	}

	private String getCollection(String baseCollection, Project project){
		return baseCollection + COLLECTION_SPARATOT + project.getId();
	}
	
	private String getCollectionNew(String baseCollection){
		return baseCollection;
	}
	/**
	 * @param collectionName
	 */
	private void createCollectionIfNotExist(String collectionName){
		if(!mongoTemplate.collectionExists(collectionName)){
			mongoTemplate.createCollection(collectionName);
		}
	}
	
	@Override
	public void saveWebData(Project project, WebData webData) {
		mongoTemplate.save(webData, getCollection(project));
	}

	@Override
	public WebData getWebData(Project project, String url) {
		return mongoTemplate.findById(url, WebData.class, getCollection(project));
	}
	
	@Override
	public Long getMediaCount(Project project, Date startDate,
			Date endDate) {
		createCollectionIfNotExist(getCollection(project));
		Query query = new Query(Criteria.where("date").gte(startDate).lte(endDate));		
		
		String map = "function () { emit( this.domain , 1 ); }";
		String reduce = "function (key, values) { return values.length; }";
		
		MapReduceResults<TermCounter> mapReduceResults = mongoTemplate
				.mapReduce(query, getCollection(project), map, reduce, TermCounter.class);
		return mapReduceResults.getCounts().getOutputCount();
	}
	
	
	@Override
	public Long getMediaMonthlyCount(Project project, Date startDate,
			Date endDate) {
		createCollectionIfNotExist(getCollection(project));
		Query query = new Query(Criteria.where("date").gte(startDate).lte(endDate));		
		
		String map = "function () { emit( this.domain , 1 ); }";
		String reduce = "function (key, values) { return values.length; }";
		
		MapReduceResults<TermCounter> mapReduceResults = mongoTemplate
				.mapReduce(query, getCollection(project), map, reduce, TermCounter.class);
		return mapReduceResults.getCounts().getOutputCount();
	}
	
	@Override
	public Long getArticleCount(Project project, Date startDate,
			Date endDate) {
		createCollectionIfNotExist(getCollection(project));
		Query query = new Query(Criteria.where("date").gte(startDate).lte(endDate));		
		Long count = mongoTemplate.count(query, getCollection(project));
		return count;
	}
	

	@Override
	public Long getArticleMonthlyCount(Project project, Date startDate,
			Date endDate) {
		createCollectionIfNotExist(getCollection(project));
		Query query = new Query(Criteria.where("date").gte(startDate).lte(endDate));		
		Long count = mongoTemplate.count(query, getCollection(project));
		return count;
	}
	
	@Override
	public List<TermCounter> getMostMedia(
			Project project,
			Date startDate, 
			Date endDate, 
			int limit, 
			int offset) {
		Aggregation aggregation = newAggregation(
				match(Criteria.where("date").gte(startDate).lte(endDate)),	
				group("domain")
				.count().as("value"),
				sort(Sort.Direction.DESC, "value"),
				skip(offset),
				limit(limit)
		);
		AggregationResults<TermCounter> result = mongoTemplate
				.aggregate(aggregation, getCollection(project), TermCounter.class);		
		return result.getMappedResults();
	}
	
	@Override
	public List<TermCounter> getMostMediaNegative(Project project,
			Date startDate, Date endDate, int limit, int offset) {
		return getMostMedia(project, WebData.NEGATIVE_SENTIMENT, 
				startDate, endDate, limit, offset);
	}
	
	@Override
	public List<TermCounter> getMostMediaPositive(Project project,
			Date startDate, Date endDate, int limit, int offset) {
		return getMostMedia(project, WebData.POSITIVE_SENTIMENT, 
				startDate, endDate, limit, offset);
	}
	
	@Override
	public List<TermCounter> getMostMediaNeutral(Project project,
			Date startDate, Date endDate, int limit, int offset) {
		return getMostMedia(project, WebData.NEUTRAL_SENTIMENT, 
				startDate, endDate, limit, offset);
	}
	
	@Override
	public List<RankCounterMedia> getMostPopularMedia(Project project,
			Date startDate, Date endDate) {
		List<RankCounterMedia> counters = new ArrayList<RankCounterMedia>();
		String outputCollection = getCollection(MEDIA_RANK_COLLECTION, project);
		if(!mongoTemplate.collectionExists(outputCollection)){
			//update data via crawler
			//updateMostPopularMedia(project, startDate, endDate);
		}
		Query queryResult = new Query().limit(MAX_MEDIA_RANK_RESULT);
		queryResult.with(new Sort(Sort.Direction.DESC, "value"));
		counters = mongoTemplate.find(queryResult, RankCounterMedia.class, outputCollection);
		return counters;
		
	}
	
	@Override
	public List<RankCounterMedia> getMostPopularPositiveMedia(
			Project project, Date startDate, Date endDate, int limit) {
		List<RankCounterMedia> counters = new ArrayList<RankCounterMedia>();
		List<RankCounterMedia> counter = new ArrayList<RankCounterMedia>();
		
		List<TermCounter> list = getMostMediaPositive(project, startDate, endDate, limit, 0);
		
		for(TermCounter termCounter : list){
			DateTime dateTime = new DateTime(new Date()).dayOfMonth().roundFloorCopy().toDateTime();
			Query query = new Query(Criteria.where("date").is(dateTime));
			query.addCriteria(Criteria.where("domain").is(termCounter.getTerm()));
			counters = mongoTemplate.find(query, RankCounterMedia.class, 
					getCollectionNew(MEDIA_RANK_COLLECTION_GLOBAL));
			if (counters.size() < 1) {
				AlexaResult result = alexaRankService.getResult(termCounter.getTerm());
				RankCounter rankCounter = new RankCounter();
				rankCounter.setDomain(termCounter.getTerm());
				rankCounter.setDate(dateTime);
				rankCounter.setCountryName(result.getCountryName());
				rankCounter.setCountryRank(result.getCountryRank());
				rankCounter.setGlobalRank(result.getPopularityRank());
				mongoTemplate.save(rankCounter, getCollectionNew(MEDIA_RANK_COLLECTION_GLOBAL));
			}
			
			RankCounter rCounter = mongoTemplate.findOne(query, RankCounter.class, getCollectionNew(MEDIA_RANK_COLLECTION_GLOBAL));
			RankCounterMedia rankCounterMedia = new RankCounterMedia();
			rankCounterMedia.setDomain(termCounter.getTerm());
			rankCounterMedia.setCountryName(rCounter.getCountryName());
			rankCounterMedia.setValue(termCounter.getValue());
			rankCounterMedia.setCountryRank(rCounter.getCountryRank());
			rankCounterMedia.setGlobalRank(rCounter.getGlobalRank());
			
			counter.add(rankCounterMedia);
		}
		return counter;
	}
	
	@Override
	public List<RankCounterMedia> getMostPopularNegativeMedia(
		Project project, Date startDate, Date endDate, int limit) {
		List<RankCounterMedia> counter = new ArrayList<RankCounterMedia>();
		List<RankCounterMedia> counters = new ArrayList<RankCounterMedia>();
		
		List<TermCounter> list = getMostMediaNegative(project, startDate, endDate, limit, 0);
		for(TermCounter termCounter : list){
			DateTime dateTime = new DateTime(new Date()).dayOfMonth().roundFloorCopy().toDateTime();
			Query query = new Query(Criteria.where("date").is(dateTime));
			query.addCriteria(Criteria.where("domain").is(termCounter.getTerm()));
			counters = mongoTemplate.find(query, RankCounterMedia.class, 
					getCollectionNew(MEDIA_RANK_COLLECTION_GLOBAL));
			if (counters.size() < 1) {
				AlexaResult result = alexaRankService.getResult(termCounter.getTerm());
				RankCounter rankCounter = new RankCounter();
				rankCounter.setDomain(termCounter.getTerm());
				rankCounter.setDate(dateTime);
				rankCounter.setCountryName(result.getCountryName());
				rankCounter.setCountryRank(result.getCountryRank());
				rankCounter.setGlobalRank(result.getPopularityRank());
				mongoTemplate.save(rankCounter, getCollectionNew(MEDIA_RANK_COLLECTION_GLOBAL));
			}
			RankCounter rCounter = mongoTemplate.findOne(query, RankCounter.class, getCollectionNew(MEDIA_RANK_COLLECTION_GLOBAL));
			RankCounterMedia rankCounterMedia = new RankCounterMedia();
			rankCounterMedia.setDomain(termCounter.getTerm());
			rankCounterMedia.setCountryName(rCounter.getCountryName());
			rankCounterMedia.setValue(termCounter.getValue());
			rankCounterMedia.setCountryRank(rCounter.getCountryRank());
			rankCounterMedia.setGlobalRank(rCounter.getGlobalRank());
			
			counter.add(rankCounterMedia);
		}		
		return counter;
	}
	
	
	@Override
	public List<PeakCounter> getPeakDay(Project project,
			Date startDate, Date endDate) {
		Aggregation aggregation = newAggregation(
				match(Criteria.where("date").gte(startDate).lte(endDate)),	
				group("peakDay")
					.count().as("value"),
				sort(Sort.Direction.ASC, previousOperation(), "peakDay")
		);
		AggregationResults<PeakCounter> results = mongoTemplate
				.aggregate(aggregation, getCollection(project), PeakCounter.class);
		return results.getMappedResults();
	}
	
	private List<TermCounter> getMostMedia(
			Project project, 
			int sentiment, 
			Date startDate, 
			Date endDate, 
			int limit, 
			int offset) {
		Aggregation aggregation = newAggregation(
				match(Criteria.where("date").gte(startDate).lte(endDate).andOperator(Criteria.where("sentiment").is(sentiment))),	
				group("domain")
				.count().as("value"),
				sort(Sort.Direction.DESC, "value"),
				skip(offset),
				limit(limit)
		);
		AggregationResults<TermCounter> result = mongoTemplate
				.aggregate(aggregation, getCollection(project), TermCounter.class);		
		return result.getMappedResults();
	}
	
	@Override
	public List<SentimentPeakCounter> getSentimentPositiveEachDay(Project project, Date startDate, Date endDate) {
		Aggregation positiveQuery = newAggregation(
				match(Criteria.where("date").gte(startDate).lte(endDate)
					.andOperator(Criteria.where("sentiment").is(WebData.POSITIVE_SENTIMENT))
				),
				group("peakDay")
					.count().as("count"),
					sort(Sort.Direction.DESC, previousOperation(), "peakDay")
			);
		AggregationResults<SentimentPeakCounter> result = mongoTemplate
				.aggregate(positiveQuery, getCollection(project), SentimentPeakCounter.class);
		return result.getMappedResults();
	}

	@Override
	public List<SentimentPeakCounter> getSentimentNegativeEachDay(Project project, Date startDate, Date endDate) {
		Aggregation negativeQuery = newAggregation(
				match(Criteria.where("date").gte(startDate).lte(endDate)
					.andOperator(Criteria.where("sentiment").is(WebData.NEGATIVE_SENTIMENT))
				),
				group("peakDay")
					.count().as("count"),
					sort(Sort.Direction.DESC, previousOperation(), "peakDay")
			);
		AggregationResults<SentimentPeakCounter> result = mongoTemplate
				.aggregate(negativeQuery, getCollection(project), SentimentPeakCounter.class);
		return result.getMappedResults();
	}

	@Override
	public List<SentimentPeakCounter> getSentimentNeutralEachDay(Project project, Date startDate, Date endDate) {
		Aggregation neutralQuery = newAggregation(
				match(Criteria.where("date").gte(startDate).lte(endDate)
					.andOperator(Criteria.where("sentiment").is(WebData.NEUTRAL_SENTIMENT))
				),
				group("peakDay")
					.count().as("count"),
					sort(Sort.Direction.DESC, previousOperation(), "peakDay")
			);
		AggregationResults<SentimentPeakCounter> result = mongoTemplate
				.aggregate(neutralQuery, getCollection(project), SentimentPeakCounter.class);
		return result.getMappedResults();
	}
	
	@Override
	public List<TermCounter> getKeywordMappingNew(Project project, Date startDate, Date endDate) {
		createCollectionIfNotExist(getCollectionNew(KEYWORD_MAPPING_COLLECTION));
			String map = "function(){" 
						   + "for(var i=0; i < this.termCounter.length; i++){"
						      + "emit(this.termCounter[i]._id, this.termCounter[i].value);"     
						    + "}"
						+"}";

			String reduce = "function(key,values){"
							    +"return Array.sum(values);"
							+"}";
			
			Query query = new Query(Criteria.where("date").gte(startDate).lte(endDate));
			query.addCriteria(Criteria.where("projectId").is(project.getId()));
			MapReduceResults<TermCounter> results = mongoTemplate.mapReduce(query, getCollectionNew(KEYWORD_MAPPING_COLLECTION), map, reduce,
					TermCounter.class);
			
			List<TermCounter> termCounter = new ArrayList<>();
			for(TermCounter term : results){		
				termCounter.add(term);
			}
			Collections.sort(termCounter);
			if(termCounter.size() > MAX_RELATED_TERMS){
				termCounter = termCounter.subList(0, MAX_RELATED_TERMS);
			}
			return termCounter;
	}
	
}
