package io.gnews.pro.report.service;

import java.io.IOException;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import io.gnews.pro.core.config.AppConfig;
import io.gnews.pro.core.config.RabbitMQConfig;
import io.gnews.pro.core.model.dto.AutomaticReportForm;


@Component
public class RabbitMQListener {
	
	@Autowired
	private AutomatedReportService arService;
	
	@RabbitListener(queues  = RabbitMQConfig.GENERATE_WEEKLY_AR)
	public void sendMailAR(@Payload AutomaticReportForm form){
		try {
			arService.generatePDFJasperChart(form.getId(), 
					form.getStart(), 
					form.getEnd(), 
					AppConfig.STATIC_ACCESSTOKEN, 
					form.getOffset(), 
					form.getLimit());
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
}
