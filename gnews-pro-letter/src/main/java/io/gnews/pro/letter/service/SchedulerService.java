package io.gnews.pro.letter.service;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


@Service
public class SchedulerService {

	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private GnewsLetterService gnewsLetterService;
	
	@Scheduled(cron = "0 0 0 * * ?")
	public void projectExpiredChecker(){
		log.info("Starting scheduled Gnews Letter Active/Disactive User By project Expired");
		gnewsLetterService.gnewsLetterActiveProjectChecker();
		
	}
		
	@Scheduled(cron = "0 30 * * * ?")
	public void gnewsLetter() {
		log.info("Starting Shceduling GNews Email Letter");
		DateTime dateTime = new DateTime();
		int hour = dateTime.getHourOfDay();
		gnewsLetterService.sendSummary(hour);
	}
	
}
	

