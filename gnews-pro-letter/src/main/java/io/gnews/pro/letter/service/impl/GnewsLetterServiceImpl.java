package io.gnews.pro.letter.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gnews.pro.core.config.RabbitMQConfig;
import io.gnews.pro.core.model.dto.MailType;
import io.gnews.pro.core.model.dto.SendEmailForm;
import io.gnews.pro.core.model.mongodb.GnewsLetter;
import io.gnews.pro.core.model.mongodb.WebData;
import io.gnews.pro.core.model.service.GnewsRestService;
import io.gnews.pro.core.repository.mongodb.GnewsLetterRepository;
import io.gnews.pro.letter.service.GnewsLetterService;

@Service
public class GnewsLetterServiceImpl implements GnewsLetterService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private GnewsLetterRepository gnewsLetterRepository;

	@Autowired
	private GnewsRestService gnewsRestService;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Override
	public void sendSummary(String keyword, String filter, String language, String to, int days) {
		Calendar calendar = Calendar.getInstance();
		Date endDate = new Date();
		calendar.setTime(endDate);
		calendar.add(Calendar.DATE, -days);
		Date startDate = calendar.getTime();
		List<WebData> list = new ArrayList<WebData>();
		int offset = 0;
		int limit = 10;
		boolean running = true;
		while (running) {
			try {
				List<WebData> articles = gnewsRestService.getData(keyword, language, filter, limit, offset);
				if (articles.size() > 0) {
					for (WebData article : articles) {
						if (article.getDate().before(endDate)) {
							if (article.getDate().after(startDate)) {
								list.add(article);
							} else {
								running = false;
								break;
							}
						}
					}
				} else {
					running = false;
				}
				offset += limit;
			} catch (Exception e) {
				log.error(e.getMessage());
				running = false;
			}
		}
		log.info("article found : " + list.size());
		if (list.size() > 0)
			try {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
				String period = "From " + simpleDateFormat.format(startDate) + " to "
						+ simpleDateFormat.format(endDate);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("keyword", keyword);
				map.put("period", period);
				map.put("articles", list);
				SendEmailForm emailForm = new SendEmailForm();
				emailForm.setType(MailType.GNEWS_LETTER);
				emailForm.setTo(to);
				emailForm.setData(map);
				rabbitTemplate.convertAndSend(RabbitMQConfig.SEND_MAIL_QUEUE, emailForm);
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		else
			System.out.println("not found new about " + keyword + " this moment");
	}

	@Override
	public void sendSummary(int hour) {
		List<GnewsLetter> emails = gnewsLetterRepository.findByHourAndActiveTrue(hour);
		System.out.println("List of Recepient" + emails);
		for (final GnewsLetter email : emails) {
			final int days = email.isFirstTime() ? 7 : 1;
			new Thread(new Runnable() {
				@Override
				public void run() {
					sendSummary(email.getProject().getKeyword(), email.getProject().getFilter(),
							String.join(",", email.getProject().getLanguanges()), email.getEmail(), days);
					log.info(email.getProject().getKeyword());
				}

			}).start();
		}
	}

	@Override
	public List<GnewsLetter> gnewsLetterActiveProjectChecker() {
		List<GnewsLetter> projectExpired = gnewsLetterRepository.findByProjectExpiredTrue();
		List<GnewsLetter> gnewsLetters = new ArrayList<>();
		for(GnewsLetter gnewsLetter : projectExpired){
			gnewsLetter.setActive(false);
			gnewsLetters.add(gnewsLetter);
		}
		log.info("Finish checking Active User for Gnews letter");
		return gnewsLetterRepository.save(gnewsLetters);
	}


}
